#ifndef VIDEOLISTMODEL_H
#define VIDEOLISTMODEL_H

#include <QAbstractListModel>
#include <QGst/Discoverer>
#include "videodata.h"
#include "gvideoplayer.h"

class VideoListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QString path READ getPath WRITE setPath NOTIFY pathChanged)
    Q_PROPERTY(QString title READ getTitle WRITE setTitle NOTIFY titleChanged)

public:
    explicit VideoListModel(QObject *parent = 0);

    enum ImageRoles {
        NameRole = Qt::UserRole + 1,
        UrlRole,
        SizeRole,
        WidthRole,
        HeightRole,
        BitrateRole,
        LengthRole,
        FramerateRole,
        VideoCodecRole,
        AudioCodecRole,
        ContainerRole
    };

    QString getPath();
    QString getTitle();
    QString getVideo();

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const;

private:
    QList<VideoData *> entryList;
    QString fullPath;
    QString title;

    QGst::DiscovererPtr discoverer;

signals:

public slots:
    void setPath(const QString &);
    void setTitle(const QString &);

Q_SIGNALS:
    void pathChanged();
    void titleChanged();
    void videoChanged();
};

#endif // VIDEOLISTMODEL_H
