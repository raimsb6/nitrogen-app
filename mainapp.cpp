#include <QQuickItem>
#include "qtquick2applicationviewer.h"
#include "mainapp.h"

MainApp::MainApp(int &argc, char **argv) :
    QGuiApplication(argc, argv)
{
    cecHandler = NULL;
    tcpHandler = NULL;
    connect(&m_settings, SIGNAL(useHdmiControlChanged()), this, SLOT(HdmiControlEnableChanged()));
    connect(&m_settings, SIGNAL(useNetworkControlChanged()), this, SLOT(NetworkControlEnableChanged()));
}

void MainApp::setViewerObject(QObject *obj)
{
    viewerObj = obj;
}

void MainApp::PrepareExec()
{
    // set settings as application wide property
    this->setProperty("settings", QVariant::fromValue<Settings *>(&m_settings));

    // get notifier object that is used to display notification text on QML
    QtQuick2ApplicationViewer *viewer = (QtQuick2ApplicationViewer *)viewerObj;
    QQuickItem *rootObj = viewer->rootObject();
    notifierObj = rootObj->findChild<QObject *>("notification");
    if (notifierObj == NULL)
        qDebug() << "cannot find child object notification";

    enableUdev();

    if (m_settings.getUseHdmiControl())
        enableCec();
    if (m_settings.getUseNetworkControl())
        enableNetwork();
}

void MainApp::PrepareClose()
{
    disableCec();
    disableNetwork();
    disableUdev();
}

void MainApp::HdmiControlEnableChanged()
{
    if (m_settings.getUseHdmiControl())
    {
        if (cecHandler == NULL)
            enableCec();
    }
    else
        disableCec();
}

void MainApp::NetworkControlEnableChanged()
{
    if (m_settings.getUseNetworkControl())
    {
        if (tcpHandler == NULL)
            enableNetwork();
    }
    else
        disableNetwork();
}

void MainApp::enableCec()
{
    if (cecHandler == NULL)
    {
        cecHandler = new CecHandler(this);
        connect(cecHandler, SIGNAL(CecConnectionEstablished(QString)), this, SLOT(CecOpened(QString)));
        connect(cecHandler, SIGNAL(CecConnectionFailed()), this, SLOT(CecOpenFailed()));
        cecHandler->Init(viewerObj);
    }
}

void MainApp::disableCec()
{
    if (cecHandler)
    {
        cecHandler->Deinit();
        delete cecHandler;
        cecHandler = NULL;
    }
}

void MainApp::enableNetwork()
{
    tcpHandler = new TcpHandler(this);
    connect(tcpHandler, SIGNAL(connectionOpened(QString)), this, SLOT(NetworkConnectionOpened(QString)));
    connect(tcpHandler, SIGNAL(connectionClosed()), this, SLOT(NetworkConnectionClosed()));
    tcpHandler->Init(viewerObj);
}

void MainApp::disableNetwork()
{
    if (tcpHandler)
    {
        tcpHandler->Deinit();
        delete tcpHandler;
        tcpHandler = NULL;
    }
}

void MainApp::enableUdev()
{
    udevHandler = new UDevHandler(&m_settings);
    udevHandler->moveToThread(&udevThread);
    connect(&udevThread, SIGNAL(started()), udevHandler, SLOT(worker()));
    connect(udevHandler, SIGNAL(Notify(QString)), this, SLOT(Notify(QString)));
    udevThread.start();
}

void MainApp::disableUdev()
{
    udevHandler->Stop();
    if (udevThread.wait(1000))
    {
        udevThread.terminate();
        udevThread.wait();
    }
}

void MainApp::Notify(QString text)
{
    if (notifierObj && text.length())
    {
        qDebug() << "NOTIFY:" << text.trimmed();
        notifierObj->setProperty("text", text.trimmed());
    }
}

void MainApp::CecOpened(QString text)
{
    Notify("Leitud CEC seade " + text);
    m_settings.hdmiConnectedStatusUpdate(true);
}

void MainApp::CecOpenFailed()
{
    Notify("Ei õnnestunud leida ühtegi HDMI CEC seadet");
}

void MainApp::NetworkConnectionOpened(QString text)
{
    m_settings.androidConnectedStatusUpdate(true);
    Notify("Võrguühendus avatud aadressilt " + text);
}

void MainApp::NetworkConnectionClosed()
{
    m_settings.androidConnectedStatusUpdate(false);
}
