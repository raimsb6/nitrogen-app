#ifndef IMAGELISTMODEL_H
#define IMAGELISTMODEL_H

#include <QtCore>
#include <QtConcurrent>
#include <QAbstractListModel>
#include "imagedata.h"

class ImageListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QString path READ getPath WRITE setPath NOTIFY pathChanged)
    Q_PROPERTY(QString title READ getTitle WRITE setTitle NOTIFY titleChanged)

public:
    explicit ImageListModel(QObject *parent = 0);

    enum ImageRoles {
            NameRole = Qt::UserRole + 1,
            UrlRole,
            ThumbRole,
            ModifiedRole,
            TakenRole,
            WidthRole,
            HeightRole,
    };

    QString getPath();
    QString getTitle();

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const;

private:
    QList<ImageData *> entryList;
    QString fullPath;
    QString title;
    QFutureWatcher<ImageData *> *imageScaling;

public Q_SLOTS:
    void showImage(int num);
    void setPath(const QString &);
    void setTitle(const QString &);
    void finished();

Q_SIGNALS:
    void pathChanged();
    void titleChanged();

};

#endif // IMAGELISTMODEL_H
