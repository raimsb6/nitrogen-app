#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QSettings>

static const QString extMediaMountPoint = "/media/sda1/";
static const QString intMediaMountPoint = "/meedia/";

class Settings : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool stickAttached READ getStickAttached WRITE setStickAttached NOTIFY stickAttachedChanged)
    Q_PROPERTY(bool useHdmiControl READ getUseHdmiControl WRITE setUseHdmiControl NOTIFY useHdmiControlChanged)
    Q_PROPERTY(bool useNetworkControl READ getUseNetworkControl WRITE setUseNetworkControl NOTIFY useNetworkControlChanged)
    Q_PROPERTY(int lastUsedSortOrder READ getLastUsedSortOrder WRITE setLastUsedSortOrder NOTIFY lastUsedSortOrderChanged)
    Q_PROPERTY(int slideshowTime READ getSlideshowTime WRITE setSlideshowTime NOTIFY slideshowTimeChanged)
    Q_PROPERTY(bool networkUp READ getNetworkUp NOTIFY networkUpChanged)
    Q_PROPERTY(QString ipAddress READ getIpAddress NOTIFY ipAddressChanged)
    Q_PROPERTY(QString netmask READ getNetmask NOTIFY netmaskChanged)
    Q_PROPERTY(QString gateway READ getGateway NOTIFY gatewayChanged)
    Q_PROPERTY(QString macAddress READ getMacAddress CONSTANT)
    Q_PROPERTY(bool hdmiConnected READ getHdmiConnected NOTIFY hdmiConnectedChanged)
    Q_PROPERTY(bool androidConnected READ getAndroidConnected NOTIFY androidConnectedChanged)

public:
    Q_INVOKABLE void refresh();

    explicit Settings(QObject *parent = 0);
    ~Settings();
    bool getStickAttached();
    bool getUseHdmiControl();
    bool getUseNetworkControl();
    int getLastUsedSortOrder();
    int getSlideshowTime();
    QString getIpAddress();
    QString getNetmask();
    QString getGateway();
    bool getNetworkUp();
    QString getMacAddress();
    bool getHdmiConnected();
    bool getAndroidConnected();

private:
    bool m_stickAttached;
    bool m_useHdmiControl;
    bool m_useNetworkControl;
    int m_lastUsedSortOrder;
    QSettings *persistentSettings;
    int m_slideshowTime;
    bool m_lastNetworkUp;
    QString m_lastIpAddress;
    QString m_lastNetmask;
    QString m_lastGateway;
    bool m_lastHdmiConnected;
    bool m_lastAndroidConnected;

signals:
    void stickAttachedChanged();
    void useHdmiControlChanged();
    void useNetworkControlChanged();
    void lastUsedSortOrderChanged();
    void slideshowTimeChanged();
    void networkUpChanged();
    void ipAddressChanged();
    void netmaskChanged();
    void gatewayChanged();
    void hdmiConnectedChanged();
    void androidConnectedChanged();

public slots:
    void setStickAttached(bool);
    void setUseHdmiControl(bool);
    void setUseNetworkControl(bool);
    void setLastUsedSortOrder(int);
    void setSlideshowTime(int);
    void hdmiConnectedStatusUpdate(bool);
    void androidConnectedStatusUpdate(bool);
};
Q_DECLARE_METATYPE(Settings *);

#endif // SETTINGS_H
