#ifndef FOLDERDATA_H
#define FOLDERDATA_H

#include <QtCore>
#include <QImage>

class FolderData
{
public:
    FolderData();

    QString label;
    QString url;
    QString thumbnail;
    QDateTime modified;
};

#endif // FOLDERDATA_H
