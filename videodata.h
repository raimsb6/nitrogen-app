#ifndef VIDEODATA_H
#define VIDEODATA_H

#include <QtCore>

class VideoData
{
public:
    VideoData();

    QString name;
    QString fileUrl;
    int sizeMb;
    int widthtPixels;
    int heightPixels;
    QString bitrate;
    QString lengthSec;
    QString framerate;
    QString videoCodec;
    QString AudioCodec;
    QString Container;
};

#endif // VIDEODATA_H
