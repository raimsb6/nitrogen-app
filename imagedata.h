#ifndef IMAGEDATA_H
#define IMAGEDATA_H

#include <QtCore>
#include <QImage>

class ImageData
{

public:
    ImageData();

    QString label;
    QString url;
    QString thumbnail;
    QDateTime modified;
    QDateTime exifTaken;
    int widthPixels;
    int heightPixels;
};

#endif // IMAGEDATA_H
