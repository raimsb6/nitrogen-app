#include "folderlistmodel.h"
#include "folderdata.h"
#include "settings.h"

FolderListModel::FolderListModel(QObject *parent) :
    QAbstractListModel(parent)
{
    topfolder = "/meedia/";
    fullPath = topfolder;
}

QHash<int, QByteArray> FolderListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NameRole] = "name";
    roles[UrlRole] = "url";
    roles[ThumbRole] = "thumb";
    roles[ModifiedRole] = "modified";
    return roles;
}

int FolderListModel::rowCount(const QModelIndex &) const
{
    return entryList.count();
}

QVariant FolderListModel::data(const QModelIndex & index, int role) const
{
    FolderData *folderData = entryList.at(index.row());
    if (role == NameRole)
        return QVariant(folderData->label);
    else if (role == UrlRole)
        return QVariant(folderData->url);
    else if (role == ModifiedRole)
        return QVariant(folderData->modified);
    else if (role == ThumbRole)
        return QVariant(folderData->thumbnail);
    return QVariant();
}

QString FolderListModel::getParentPath()
{
    QDir dir(fullPath);
    if (dir.cdUp())
        return dir.absolutePath() + "/";
    else
        return topfolder;
}

QString FolderListModel::getPath()
{
    return fullPath;
}

void FolderListModel::setPath(const QString &path)
{
    if (path != fullPath)
    {
        beginResetModel();
        fullPath = path;
        QDir dir;
        QDir parentdir(fullPath);
        parentdir.cdUp();
        dir.setPath(fullPath);
        qDeleteAll(entryList);
        entryList.clear();

        FolderData *folder;
        if (fullPath != topfolder) {
            folder = new FolderData();
            folder->label = "Tagasi kausta " + parentdir.dirName();
            folder->thumbnail = "qrc:/images/images/arrow-up.png";
            folder->url = parentdir.absolutePath() + "/";
            entryList.append(folder);
        }

        QStringList filters;
        filters << "*.jpg" << "*.bmp" << "*.png";
        numphotos = dir.entryList(filters).count();
        filters.clear();
        filters << "*.mpg" << "*.mpeg4" << "*.mp4" << "*.avi" << "*.mov" << "*.mkv";
        numvideos = dir.entryList(filters).count();

        if (numphotos > 0)
        {
            folder = new FolderData();
            folder->label = "Fotod";
            folder->thumbnail = "qrc:/images/images/images.png";
            folder->url = "photos";
            entryList.append(folder);
        }

        if (numvideos > 0)
        {
            folder = new FolderData();
            folder->label = "Videod";
            folder->thumbnail = "qrc:/images/images/video.png";
            folder->url = "videos";
            entryList.append(folder);
        }
        Settings *settings = qApp->property("settings").value<Settings *>();
        if (settings->getStickAttached() && dir.absolutePath().startsWith(extMediaMountPoint))
        {
            folder = new FolderData();
            folder->label = "Kopeeri kaust mälusse";
            folder->thumbnail = "qrc:/images/images/download.png";
            folder->url = "download";
            entryList.append(folder);
        }
        else if (settings->getStickAttached() && (numphotos + numvideos > 0))
        {
            folder = new FolderData();
            folder->label = "Kopeeri kaust mälukaardile";
            folder->thumbnail = "qrc:/images/images/upload.png";
            folder->url = "upload";
            entryList.append(folder);
        }
        if (fullPath != topfolder)
        {
            folder = new FolderData();
            folder->label = "Kustuta kaust";
            folder->thumbnail = "qrc:/images/images/delete.png";
            folder->url = "delete";
            entryList.append(folder);
        }

       setTitle(dir.absolutePath());

       foreach (QString file, dir.entryList()) {
           QFileInfo fileinfo(fullPath + file);

           if (fileinfo.isDir() && fileinfo.fileName() != "." && fileinfo.fileName() != "..") {
               QDir tmpdir(fileinfo.absoluteFilePath());
               folder = new FolderData();
               folder->label = tmpdir.dirName();
               if (folder->label.length() > 39)
                   folder->label = folder->label.left(36) + "...";
               folder->thumbnail = "qrc:/images/images/folder.png";
               folder->url = tmpdir.absolutePath() + "/";
               folder->modified = fileinfo.lastModified();
               entryList.append(folder);
           }
       }
       endResetModel();
       emit pathChanged();
    }
}

void FolderListModel::setTitle(const QString &newTitle)
{
   if (newTitle != title) {
       title = newTitle;
       emit titleChanged();
   }
}

QString FolderListModel::getTitle()
{
    return title;
}

int FolderListModel::getNumPhotos()
{
    return numphotos;
}

int FolderListModel::getNumVideos()
{
    return numvideos;
}

QString FolderListModel::getTopFolder()
{
    return topfolder;
}

void FolderListModel::setTopFolder(const QString &newTopFolder)
{
    if (newTopFolder != topfolder)
    {
        topfolder = newTopFolder;
        emit topFolderChanged();
    }
}

bool FolderListModel::remove(QString path)
{
    QDir dir(path);
    dir.removeRecursively();
    qDebug() << "remove dir" << dir.absolutePath();
    return true;
}
