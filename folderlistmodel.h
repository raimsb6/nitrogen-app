#ifndef FOLDERLISTMODEL_H
#define FOLDERLISTMODEL_H

#include <QAbstractListModel>
#include "folderdata.h"

class FolderListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QString path READ getPath WRITE setPath NOTIFY pathChanged)
    Q_PROPERTY(QString title READ getTitle WRITE setTitle NOTIFY titleChanged)
    Q_PROPERTY(int numPhotos READ getNumPhotos CONSTANT)
    Q_PROPERTY(int numVideos READ getNumVideos CONSTANT)
    Q_PROPERTY(QString topFolder READ getTopFolder WRITE setTopFolder NOTIFY topFolderChanged)
    Q_PROPERTY(QString parentPath READ getParentPath)

public:
    Q_INVOKABLE bool remove(QString path);

    explicit FolderListModel(QObject *parent = 0);

    enum ImageRoles {
            NameRole = Qt::UserRole + 1,
            UrlRole,
            ThumbRole,
            ModifiedRole,
    };

    QString getPath();
    QString getParentPath();
    QString getTitle();
    int getNumVideos();
    int getNumPhotos();
    QString getTopFolder();

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const;

private:
    QList<FolderData *> entryList;
    QString fullPath;
    QString title;
    int numvideos;
    int numphotos;
    QString topfolder;

//public Q_SLOTS:
public slots:
    void setPath(const QString &);
    void setTitle(const QString &);
    void setTopFolder(const QString &);

//Q_SIGNALS:
signals:
    void pathChanged();
    void titleChanged();
    void topFolderChanged();

public slots:

};

#endif // FOLDERLISTMODEL_H
