#include <QUrl>
#include "advancedimage.h"

AdvancedImage::AdvancedImage(QQuickItem *parent) :
    QQuickPaintedItem(parent)
{
    connect(&m_futurewatcher, SIGNAL(finished()), this, SLOT(scaleDone()));
}

void AdvancedImage::paint(QPainter *painter)
{
    // center image
    int x = 0, y = 0;
    if (m_pixmap.width() < width())
        x = (width() - m_pixmap.width()) / 2;
    if (m_pixmap.height() < height())
        y = (height() - m_pixmap.height()) / 2;
    // draw
    painter->drawPixmap(x, y, m_pixmap);
}

QString AdvancedImage::getSource()
{
    return m_source;
}

void AdvancedImage::setSource(QString newSource)
{
    if (newSource != m_source) {
        m_source = newSource;
        if (m_future.isRunning())
            m_future.cancel();
        m_future = QtConcurrent::run(this, &AdvancedImage::scaleImage);
        m_futurewatcher.setFuture(m_future);
        emit sourceChanged();
    }
}

void AdvancedImage::scaleImage()
{
    QPixmap tmpPixmap;
    QUrl url = m_source;

    if (tmpPixmap.load(url.toLocalFile()) == true) {
        m_pixmap = tmpPixmap.scaled(width(), height(), Qt::KeepAspectRatio);
    }
}

void AdvancedImage::scaleDone()
{
    update();
}
