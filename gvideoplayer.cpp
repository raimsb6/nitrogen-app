/*
   Copyright (C) 2010 Marco Ballesio <gibrovacco@gmail.com>
   Copyright (C) 2011-2013 Collabora Ltd.
     @author George Kiagiadakis <george.kiagiadakis@collabora.co.uk>

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published
   by the Free Software Foundation; either version 2.1 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "gvideoplayer.h"
#include <QUrl>
#include <QDebug>
#include <QQuickItem>
#include <QGlib/Connect>
#include <QGlib/Error>
#include <QGst/ElementFactory>
#include <QGst/Bus>

GVideoPlayer::GVideoPlayer(QObject *parent)
    : QQuickItem()
{
}

void GVideoPlayer::setVideoSink(const QGst::ElementPtr & sink)
{
    m_videoSink = sink;
}

void GVideoPlayer::play()
{
    qDebug() << "play";
    if (m_pipeline.isNull())
        createPipeline();
     m_pipeline->setState(QGst::StatePlaying);

}

void GVideoPlayer::pause()
{
    if (!m_pipeline.isNull()) {
        m_pipeline->setState(QGst::StatePaused);
    }
}

void GVideoPlayer::stop()
{
    qDebug() << "stop";
    if (!m_pipeline.isNull()) {
        m_pipeline->setState(QGst::StateNull);
        m_pipeline.clear();
        m_videoSink.clear();
    }
}

QString GVideoPlayer::getUri()
{
    return m_videoUri;
}

void GVideoPlayer::createPipeline()
{
    if (!m_videoSink) {
        qDebug() << "create pipeline";
        m_videoSink = QGst::ElementFactory::make("mfw_v4lsink");
        m_videoSink->setProperty("force-aspect-ratio", true);
        if (m_fullscreen)
        {
            m_videoSink->setProperty("axis-left", 0);
            m_videoSink->setProperty("axis-top", 0);
            m_videoSink->setProperty("disp-width", 1920);
            m_videoSink->setProperty("disp-height", 1080);
        }
        else
        {
            m_videoSink->setProperty("axis-left", x());
            m_videoSink->setProperty("axis-top", y());
            m_videoSink->setProperty("disp-width", width());
            m_videoSink->setProperty("disp-height", height());
        }
        m_audioSink = QGst::ElementFactory::make("alsasink");
        m_audioSink->setProperty("device", "hw:1,0");
    }
    if (m_pipeline.isNull()) {
        m_pipeline = QGst::ElementFactory::make("playbin2").dynamicCast<QGst::Pipeline>();
        if (!m_pipeline.isNull()) {
            m_pipeline->setProperty("video-sink", m_videoSink);
            m_pipeline->setProperty("audio-sink", m_audioSink);
            m_pipeline->setProperty("uri", "file://" + m_videoUri);

            //watch the bus for messages
            QGst::BusPtr bus = m_pipeline->bus();
            bus->addSignalWatch();
            QGlib::connect(bus, "message", this, &GVideoPlayer::onBusMessage);
        } else {
            qCritical() << "Failed to create the pipeline";
        }
    }
}

void GVideoPlayer::setUri(const QString & uri)
{
    if (uri == m_videoUri)
        return;

    qDebug() << "uri changed to" << uri;

    if (m_pipeline.isNull())
        createPipeline();

    m_pipeline->setProperty("uri", "file://" + uri);
    m_videoUri = uri;
    emit uriChanged();
}

void GVideoPlayer::onBusMessage(const QGst::MessagePtr & message)
{
    switch (message->type()) {
    case QGst::MessageEos: //End of stream. We reached the end of the file.
        stop();
        break;
    case QGst::MessageError: //Some error occurred.
        qCritical() << message.staticCast<QGst::ErrorMessage>()->error();
        stop();
        break;
    default:
        break;
    }
}

bool GVideoPlayer::getFullscreen()
{
    return m_fullscreen;
}

void GVideoPlayer::setFullscreen(bool fullscreen)
{
    if (m_fullscreen != fullscreen)
    {
        if (!m_pipeline.isNull())
        {
            if (fullscreen == true)
            {
                qDebug() << "fullscreen";
                m_videoSink->setProperty("axis-left", 0);
                m_videoSink->setProperty("axis-top", 0);
                m_videoSink->setProperty("disp-width", 1920);
                m_videoSink->setProperty("disp-height", 1080);
            }
            else
            {
                qDebug() << "x:" << x() << " y:" << y() << " " << width() << "x" << height();
                m_videoSink->setProperty("axis-left", x());
                m_videoSink->setProperty("axis-top", y());
                m_videoSink->setProperty("disp-width", width());
                m_videoSink->setProperty("disp-height", height());
            }
        }
        m_fullscreen = fullscreen;
        emit fullscreenChanged();
    }
}

//#include "moc_player.cpp"
