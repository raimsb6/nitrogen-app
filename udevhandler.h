#ifndef UDEVHANDLER_H
#define UDEVHANDLER_H

#include <QObject>
#include "settings.h"

class UDevHandler : public QObject
{
    Q_OBJECT
public:
    explicit UDevHandler(Settings *settings, QObject *parent = 0);
    void Stop();

private:
    QString getMountEntry(QString deviceNode);
    bool getUsbAttached(struct udev *pUdev);
    Settings *m_settings;
    bool m_stopRequested;

signals:
    void Notify(QString message);

public slots:
    void worker();

};

#endif // UDEVHANDLER_H
