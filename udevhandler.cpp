#include <QDebug>
#include <libudev.h>
#include "udevhandler.h"
#include <fcntl.h>
#include <mntent.h>

UDevHandler::UDevHandler(Settings *settings, QObject *parent) :
    QObject(parent)
{
    m_settings = settings;
    m_stopRequested = false;
}

void UDevHandler::worker()
{
    struct udev* uDev = udev_new();
    if (uDev == NULL)
    {
        qDebug() << "udev new failed";
        return;
    }

    m_settings->setStickAttached(getUsbAttached(uDev));
    qDebug() << "Setting memory stick attached:" << m_settings->getStickAttached();

    struct udev_monitor* udevMon = udev_monitor_new_from_netlink(uDev, "udev");
    if (udevMon == NULL)
    {
        qDebug() << "udev monitor failed";
        return;
    }
    // make monitor blocking
    int saved_flags = fcntl(udev_monitor_get_fd(udevMon), F_GETFL);
    fcntl(udev_monitor_get_fd(udevMon), F_SETFL, saved_flags & ~O_NONBLOCK);

    udev_monitor_filter_add_match_subsystem_devtype(udevMon, "block", "disk");
    udev_monitor_enable_receiving(udevMon);

    struct udev_device *pDev;
    qDebug() << "thread running";
    while (!m_stopRequested)
    {
        pDev = udev_monitor_receive_device(udevMon);
        if (pDev)
        {
            qDebug() << "got udev event";
            QString text;
            QString action = QString::fromUtf8(udev_device_get_action(pDev));
            pDev = udev_device_get_parent_with_subsystem_devtype(pDev, "scsi", "scsi_device");
            if (pDev)
            {
                qDebug() << "got event usb dev";
                QString vendor = QString::fromUtf8(udev_device_get_sysattr_value(pDev,"vendor"));
                QString model = QString::fromUtf8(udev_device_get_sysattr_value(pDev,"model"));

                if (action == "add")
                {
                    text = QString("Mälupulk ühendatud: %1 %2").arg(vendor.trimmed(), model.trimmed());
                    m_settings->setStickAttached(true);
                }
            }
            else
            {
                if (action == "remove")
                {
                    text = "Mälupulk eemaldatud";
                    m_settings->setStickAttached(false);
                }
            }
            Notify(text);
            udev_device_unref(pDev);
        }
        else {
            qDebug() << "No Device from receive_device(). An error occured.";

        }
    }

    udev_monitor_unref(udevMon);
    udev_unref(uDev);
    qDebug() << "thread exiting";
}

void UDevHandler::Stop()
{
    m_stopRequested = true;
}

bool UDevHandler::getUsbAttached(struct udev *pUdev)
{
    struct udev_enumerate *enumerate;
    struct udev_list_entry *devices, *dev_list_entry;
    struct udev_device *dev;
    bool ret = false;

    enumerate = udev_enumerate_new(pUdev);
    udev_enumerate_add_match_subsystem(enumerate, "block");
    udev_enumerate_scan_devices(enumerate);
    devices = udev_enumerate_get_list_entry(enumerate);
    udev_list_entry_foreach(dev_list_entry, devices)
    {
        const char *path;
        path = udev_list_entry_get_name(dev_list_entry);
        dev = udev_device_new_from_syspath(pUdev, path);
        if (dev)
        {
            dev = udev_device_get_parent_with_subsystem_devtype(dev, "usb", "usb_device");
            if (dev)
            {
                ret = true;
                udev_device_unref(dev);
            }
        }
    }
    udev_enumerate_unref(enumerate);
    return ret;
}

QString UDevHandler::getMountEntry(QString deviceNode)
{
    QString ret = "";
    FILE *mt = setmntent("/etc/mtab", "r");
    if (mt == NULL)
        return ret;
    struct mntent *mntentry;
    QString mntpnt;
    do
    {
        mntentry = getmntent(mt);
        if (mntentry == NULL)
            break;
        mntpnt = QString::fromLocal8Bit(mntentry->mnt_fsname);
        qDebug() << "Mnt_dir: " << mntentry->mnt_dir;
        qDebug() << "mnt_fsname:"<<mntentry->mnt_fsname;
    } while (mntentry != NULL && mntpnt != deviceNode);

    if (mntentry != NULL)
        ret = QString(mntentry->mnt_dir);
    endmntent(mt);
    return ret;
}
