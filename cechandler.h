#ifndef CECHANDLER_H
#define CECHANDLER_H

#include <QObject>
#include <QtConcurrent>
#include <libcec/cec.h>

using namespace CEC;

class CecHandler : public QObject
{
    Q_OBJECT
public:
    explicit CecHandler(QObject *parent = 0);
    ~CecHandler();
    bool Init(QObject *viewer);
    QObject *getViewer();
    void Deinit();

private:
    ICECAdapter *m_adapter;
    ICECCallbacks m_callbacks;
    QFuture<bool> future;
    QFutureWatcher<bool> futureWatcher;
    QObject *viewerObj;

    static int cecLogMessage(void *ptr, const cec_log_message msg);
    static int cecKeyPress(void *ptr, const cec_keypress press);
    static int cecCommand(void *ptr, const cec_command cmd);
    static int cecConfChange(void *ptr, const libcec_configuration conf);
    static int cecAlert(void *ptr, const libcec_alert alert, const libcec_parameter param);
    static int cecMenuStateChanged(void *ptr, const cec_menu_state state);
    static void cecSourceActivated(void *ptr, const cec_logical_address addr, const uint8_t);
    static void postKeyPress(QObject *receiver, int keycode);

    bool OpenAdapter(QString port);

signals:
    void CecConnectionFailed();
    void CecConnectionEstablished(QString deviceName);

public slots:
    void CecConnectionOpenFinished();
};

#endif // CECHANDLER_H
