#ifndef MAINAPP_H
#define MAINAPP_H

#include <QGuiApplication>
#include <QThread>
#include "settings.h"
#include "cechandler.h"
#include "tcphandler.h"
#include "udevhandler.h"

class MainApp : public QGuiApplication
{
    Q_OBJECT
public:
    explicit MainApp(int &argc, char **argvs);
    Settings m_settings;
    void PrepareExec();
    void PrepareClose();
    void setViewerObject(QObject *obj);

private:
    CecHandler *cecHandler;
    TcpHandler *tcpHandler;
    UDevHandler *udevHandler;
    QThread udevThread;
    QObject *viewerObj;
    QObject *notifierObj;

    void enableCec();
    void disableCec();
    void enableNetwork();
    void disableNetwork();
    void enableUdev();
    void disableUdev();

signals:

public slots:
    void HdmiControlEnableChanged();
    void NetworkControlEnableChanged();
    void Notify(QString text);
    void CecOpened(QString text);
    void CecOpenFailed();
    void NetworkConnectionOpened(QString text);
    void NetworkConnectionClosed();
};

#endif // MAINAPP_H
