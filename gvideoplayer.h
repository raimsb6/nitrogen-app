#ifndef GVIDEOPLAYER_H
#define GVIDEOPLAYER_H

#include <QObject>
#include <QQuickItem>
#include <QGst/Pipeline>
#include <QGst/Message>

class GVideoPlayer : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(bool fullscreen READ getFullscreen WRITE setFullscreen NOTIFY fullscreenChanged)
    Q_PROPERTY(QString uri READ getUri WRITE setUri NOTIFY uriChanged)

public:
    Q_INVOKABLE void play();
    Q_INVOKABLE void pause();
    Q_INVOKABLE void stop();

    explicit GVideoPlayer(QObject *parent = 0);

    void setVideoSink(const QGst::ElementPtr & sink);
    bool getFullscreen();
    void setFullscreen(bool fullscreen);
    QString getUri();
    void setUri(const QString &);

private:
    void onBusMessage(const QGst::MessagePtr & message);
    void createPipeline();

    QGst::PipelinePtr m_pipeline;
    QGst::ElementPtr m_videoSink;
    QGst::ElementPtr m_audioSink;
    bool m_fullscreen;
    QString m_videoUri;

Q_SIGNALS:
    void uriChanged();
    void fullscreenChanged();
};

#endif // GVIDEOPLAYER_H
