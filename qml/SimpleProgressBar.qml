import QtQuick 2.0

Item {
    id: progressbar

    property int minimum: 0
    property int maximum: 100
    property int value: 0
    property color color: "#77B753"
    property alias animationEnabled: behaviour.enabled

    width: 250; height: 24
    clip: true

    Rectangle {
        id: border
        anchors.fill: parent
        anchors.bottomMargin: 1
        anchors.rightMargin: 1
        color: "transparent"
        border.width: 2
        border.color: "#212121"
    }

    Rectangle {
        id: highlight
        property int widthDest: ( ( progressbar.width * ( value - minimum ) ) / ( maximum - minimum ) - 5 )
        width: highlight.widthDest

        Behavior on width {
            id: behaviour
            SmoothedAnimation {
                velocity: 1200
            }
        }
        color: "#2424c7"

        anchors {
            left: parent.left
            top: parent.top
            bottom: parent.bottom
            margins: 2
        }
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#5555ff" }
            GradientStop { position: 1.0; color: "#5f5fc4" }
                //GradientStop { position: 0.0; color: "#ffffff" }
                //GradientStop { position: 1.0; color: "#aaaaaa" }
        }
    }
}
