import QtQuick 2.0
import QtQuick.Layouts 1.0

    Rectangle {
        property alias image: image.source
        property alias text: label.text
        property alias color: remotecolor.color
        signal pressed

        id: button

        width: 300
        height: 300
        radius: 10
        border.width: 7
        state: "normal"
        gradient: Gradient {
                GradientStop { position: 0.0; color: "#b0c5de" }
                GradientStop { position: 1.0; color: "darkblue" }
            }

        Image {
            id: image
            scale: parent.width / 300
            fillMode: Image.Pad
            anchors.fill: parent
        }

        MouseArea {
            anchors.fill: parent
            onEntered: button.state = "active"
            onExited: button.state = "normal"
            hoverEnabled: true
            onClicked: button.state = "pressed";
        }

        states: [
            State {
                name: "hidden"
                PropertyChanges { target: button; opacity: 0.9 }
                PropertyChanges { target: button; scale: 0.0 }
            },
            State {
                name: "normal"
                PropertyChanges { target: button; border.color: "white" }
                PropertyChanges { target: button; opacity: 0.7 }
                PropertyChanges { target: button; scale: 1.0 }
            },
            State {
                name: "active"
                PropertyChanges { target: button; border.color: "red" }
                PropertyChanges { target: button; opacity: 0.9 }
            },
            State {
                name: "pressed"
                PropertyChanges { target: button; border.color: "red" }
                PropertyChanges { target: button; scale: 1.0 }
                PropertyChanges { target: button; opacity: 0.9 }
            }
        ]

        transitions: [
            Transition {
                from: "normal"; to: "hidden"
                ParallelAnimation {
                    PropertyAnimation { property: "opacity"; to: 0.0; duration: 200 }
                    PropertyAnimation { property: "scale"; to: 0.1; duration: 200 }
                    PropertyAction { target: button; property: "visible"; value: "false" }
                }
            },
            Transition {
                from: "hidden"; to: "normal"
                ParallelAnimation {
                    PropertyAction { target: button; property: "visible"; value: "true" }
                    PropertyAnimation { property: "opacity"; to: 0.9; duration: 200 }
                    PropertyAnimation { property: "scale"; to: 1.0; duration: 200 }
                }
            },
            Transition {
                from: "normal"; to: "active"
                ColorAnimation { from: "white"; to: "red"; duration: 100 }
            },
            Transition {
                from: "active"; to: "normal"
                ColorAnimation { from: "red"; to: "white"; duration: 100 }
            },
            Transition {
                from: "active"; to: "pressed"
                SequentialAnimation {
                    PropertyAnimation { property: "scale"; to: 1.1; duration: 25; }
                    PropertyAnimation { property: "scale"; to: 1.0; duration: 50 }
                }
                // Wait until animation has finished before emitting signal
                onRunningChanged: {
                    if (!running)
                        button.pressed()
                }
            }
        ]

        RowLayout {
            anchors.horizontalCenter: button.horizontalCenter
            y: button.y + button.height
            spacing: 10
            height: 30

            Rectangle {
                id: remotecolor
                width: 20
                height: 8
            }

            Text {
                id: label
                width: parent.width
                color: "white"
                font.pointSize: 18
                font.family: "Verdana"
            }
        }
    }

