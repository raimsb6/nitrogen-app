import QtQuick 2.0
//import MyLibrary 1.0

Rectangle {
    property alias text: label.text
    property alias color: remotecolor.color
    signal pressed
    
    id: button
    
    width: 300
    height: 300
    radius: 2
    border.width: 2
    state: "normal"
    gradient: Gradient {
        GradientStop { position: 0.0; color: "lightsteelblue" }
        GradientStop { position: 1.0; color: "darkblue" }
    }

    Keys.onDigit1Pressed: {
        if (color == "red") {
            button.state = "pressed";
        }
    }
    
    MouseArea {
        anchors.fill: parent
        onEntered: button.state = "active"
        onExited: button.state = "normal"
        hoverEnabled: true
        onClicked: button.state = "pressed";
    }
    
    Rectangle {
        id: remotecolor
        x: parent.x + 15
        width: 20
        height: 8
        anchors.verticalCenter: parent.verticalCenter
    }

    Text {
        id: label
        x: parent.x + 40
        width: parent.width
        color: "white"
        font.pointSize: 18
        font.family: "OpenSans"
        font.letterSpacing: 0
        anchors.top: parent.top
        anchors.leftMargin: 10
        anchors.topMargin: 8
    }
    
    states: [
        State {
            name: "normal"
            PropertyChanges { target: button; border.color: "white" }
            PropertyChanges { target: button; opacity: 0.7 }
        },
        State {
            name: "active"
            PropertyChanges { target: button; border.color: "red" }
            PropertyChanges { target: button; opacity: 0.9 }
        },
        State {
            name: "pressed"
            PropertyChanges { target: button; border.color: "red" }
            PropertyChanges { target: button; scale: 1.0 }
            PropertyChanges { target: button; opacity: 0.9 }
        }
    ]
    
    transitions: [
        Transition {
            from: "normal"; to: "active"
            ColorAnimation { from: "white"; to: "red"; duration: 200 }
        },
        Transition {
            from: "active"; to: "normal"
            ColorAnimation { from: "red"; to: "white"; duration: 200 }
        },
        Transition {
            from: "active"; to: "pressed"
            SequentialAnimation {
                PropertyAnimation { property: "scale"; to: 1.1; duration: 50; }
                PropertyAnimation { property: "scale"; to: 1.0; duration: 150 }
            }
            // Wait until animation has finished before emitting signal
            onRunningChanged: {
                if (!running)
                    button.pressed()
            }
        }
    ]
}
