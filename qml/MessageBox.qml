import QtQuick 2.0

Dialog {
    property alias text: tekst.text
    signal closed()
    id: dialog
    title: "Teade"
    width: 800
    height: 500

    Text {
        id: tekst
        y: 150
        x: 50
        color: "white"
        wrapMode: Text.WordWrap
        text: "test tekst pikk jutt sitt jutt"
        font.pixelSize: 40
    }

    onVisibleChanged: if(visible) closeButton.focus = true;

    DialogButton {
        id: closeButton
        anchors.horizontalCenter: parent.horizontalCenter
        y: 300
        text: "OK"
        state: "selected"

        onPressed: {
            console.log("messagebox close");
            closed();
        }

    }
}
