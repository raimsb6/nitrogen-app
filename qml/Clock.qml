import QtQuick 2.0

Rectangle {

    Text {
        anchors.centerIn: parent
        color: "white"
        text: Qt.formatDateTime(new Date(), "HH:mm")
        font.pointSize: 24

        Timer {
            interval: 30000
            running: true
            repeat: true
            onTriggered: parent.text = Qt.formatDateTime(new Date(), "HH:mm")
        }
    }
}
