import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {
    property string title
    x: 1920 / 2 - width / 2
    y: 1080 / 2 - height / 2

    Item {
        id: container
        visible: false
        anchors.centerIn: parent
        width: parent.width
        height: parent.height

        Rectangle {
            id: dialog
            width: parent.width - 20
            height: parent.height - 20
            anchors.centerIn: parent
            visible: true
            radius: 2
            //border.color: "#212121"
            border.color: "white"
            border.width: 2
            color: "#414141"
           // gradient: Gradient {
             //       GradientStop { position: 0.0; color: "#414141" }
               //     GradientStop { position: 1.0; color: "#212121" }
            //}

            Rectangle {
                x: 0
                y: 0
                width: parent.width
                height: 50
                border.color: "white"
                color: "transparent"

                Text {
                    anchors.centerIn: parent
                    color: "white"
                    font.pixelSize: 40
                    text: title
                }
            }
        }
    }

    DropShadow {
        id: rectShadow
        anchors.fill: container
        horizontalOffset: 20
        verticalOffset: 20
        radius: 16
        samples: 16
        spread: 0
        color: "#222222"
        //smooth: true
        opacity: 0.7
        source: container
        visible: true
    }
}
