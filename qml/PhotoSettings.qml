import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {
    signal exited
    property string folderPath
    property string toplevel

    anchors.fill: parent

    Item {
        id: content

        Text {
            x: 500
            y: 250
            text: "Pildi seaded"
            font.family: "Verdana"
            font.pointSize: 50
            color: "#757add"
        }

        ListModel {
            id: statusMenuModel
            ListElement {
                identifier: "id1"
                name: "Piltide sorteerimine"
            }
            ListElement {
                identifier: "id2"
                name: "Slaidiesituse intervall"
            }
            function value(identifier) {
                if (identifier === "id1") {
                    var order = GlobalSettings.lastUsedSortOrder;
                    if (order == 0)
                        return "A-Ü";
                    else if (order == 1)
                        return "Ü-A";
                    else if (order == 2)
                        return "Muudetd enne";
                    else if (order == 3)
                        return "Muudetud lõpus";
                    else if (order == 4)
                        return "Vanemad ees";
                    else if (order == 5)
                        return "Uuemad ees";
                }
                else if (identifier === "id2") {
                    if (GlobalSettings.slideshowTime == 0)
                        return "Käsitsi";
                    else
                        return GlobalSettings.slideshowTime + " sek";
                }
                else
                    return "?";
            }
        }
        Timer {
            interval: 1000
            repeat: true
            running: true
            onTriggered: {
                GlobalSettings.refresh();
            }
        }

        ListView {
            id: menuList
            x: 550
            y: 400
            width: 400
            height: 500
            model: statusMenuModel
            focus: true
            currentIndex: 0
            highlightFollowsCurrentItem: true
            highlightMoveDuration: 50

            Keys.onEscapePressed: {
                console.log("status exit");
                exited();
            }
            Keys.onReturnPressed: {
                if (currentItem.myData.identifier === "id1")
                    GlobalSettings.lastUsedSortOrder = (GlobalSettings.lastUsedSortOrder + 1) % 6;
                else if (currentItem.myData.identifier === "id2")
                    GlobalSettings.slideshowTime = (GlobalSettings.slideshowTime + 1) % 10;
            }

            delegate: Item {
                property variant myData: model
                id: item1
                width: 800
                height: 70
                Text {
                    text: model.name
                    anchors.left: parent.left
                    anchors.leftMargin: 20
                    anchors.top: parent.top
                    anchors.topMargin: 6
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    font.family: "Verdana"
                    font.pointSize: 30
                    antialiasing: true
                }

                Text {
                    text: statusMenuModel.value(model.identifier)
                    anchors.top: parent.top
                    anchors.topMargin: 6
                    anchors.right: parent.right
                    anchors.rightMargin: 20

                    horizontalAlignment: Text.AlignRight
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    font.family: "Verdana"
                    font.pointSize: 30
                    antialiasing: true
                }

                Rectangle {
                    y: 68
                    width: 780
                    height: 2
                    radius: 5
                    anchors.leftMargin: 10
                    anchors.left: parent.left
                    color: "#757add"
                }
            }
            highlight: Item {

                RectangularGlow {
                    id: effect
                    width: 800
                    height: 68
                    glowRadius: 30
                    spread: 0.0
                    color: "#393380"
                    cornerRadius: 25
                }
                Rectangle {
                    id: jura
                    y: 0
                    x: 0
                    width: 800
                    height: 68
                    radius: 10
                    gradient: Gradient {
                        GradientStop {
                            position: 0.641
                            color: "#00000000"
                        }

                        GradientStop {
                            position: 0.149
                            color: "#c2c2c2"
                        }

                        GradientStop {
                            position: 0.45
                            color: "#ffffff"
                        }

                    }
                    border.color: "white"
                    clip: false
                    border.width: 2
                    opacity: 0.4
                    smooth: true
                    antialiasing: true
                }
            }
        }
    }
}
