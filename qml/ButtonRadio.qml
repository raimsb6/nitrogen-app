import QtQuick 2.0

Item {
    id: sideButton
    property string text: 'Button'
    property RadioGroup radioGroup
    property int radioIndex

    MouseArea {
        id: sideButtonMouseArea
        anchors.fill: parent
        hoverEnabled: true
        onClicked: { sideButton.radioGroup.selectedIndex = sideButton.radioIndex }
    }

    Rectangle {
        id: sideRectangle
        color: "lightgray"
        width: 30
        height: 30
        radius: 4

        Rectangle {
            width: parent.width - 16
            height: parent.height - 16
            radius: 4
            color: "darkblue"
            anchors.centerIn: parent
            visible: radioGroup.selectedIndex === radioIndex ? true : false
        }
    }

    Text {
        id: sideButtonLabel
        text: sideButton.text
        font.pixelSize: 25
        font.family: 'OpenSans'
        anchors.left: sideRectangle.right
        anchors.leftMargin:     15
        color:  radioGroup.selectedIndex === sideButton.radioIndex ? '#E2EBFC' :
                    (sideButtonMouseArea.containsMouse ? "red" : '#787878')
    }
}
