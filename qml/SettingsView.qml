import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {
    id: settingsView
    signal exited
    property string folderPath
    property string toplevel

    anchors.fill: parent

    Keys.onEscapePressed: {
        exited();
    }

    Loader {
        id: settingLoader

        onLoaded: {
            content.visible = false;
            item.visible = true;
            focus = true;
        }
    }
    Connections {
            id: menuConnection
            target: settingLoader.item
            ignoreUnknownSignals: true
            onExited:
            {
                settingLoader.source = "";
                content.visible = true;
                menuList.focus = true;

            }
    }

    Item {
        id: content

        Text {
            x: 500
            y: 250
            text: "Seaded"
            font.family: "Verdana"
            font.pointSize: 50
            color: "#757add"
        }

        ListModel {
            id: settingsMenuModel
            ListElement {
                identifier: "id1"
                name: "Süsteemi olek"
                qml: "SystemStatus.qml"
            }
            ListElement {
                identifier: "id2"
                name: "Süsteemi seaded"
                qml: "SystemSettings.qml"
            }
            ListElement {
                identifier: "id3"
                name: "Pildi seaded"
                qml: "PhotoSettings.qml"
            }
            ListElement {
                identifier: "id4"
                name: "Video seaded"
                qml: "VideoSettings.qml"
            }
            ListElement {
                identifier: "id5"
                name: "DLNA seaded"
                qml: "DlnaSettings.qml"
            }
            ListElement {
                identifier: "id6"
                name: "Nuppude seadistus"
                qml: "InputSettings.qml"
            }
            ListElement {
                identifier: "exit"
                name: "Tagasi"
            }
        }



        ListView {
            id: menuList
            x: 550
            y: 400
            width: 400
            height: 500
            keyNavigationWraps: true
            model: settingsMenuModel
            highlightFollowsCurrentItem: true
            highlightMoveDuration: 50
            focus: true
            currentIndex: 0

            Keys.onDownPressed: { if (currentIndex < (count - 1)) currentIndex++ }
            Keys.onUpPressed: { if (currentIndex > 0) currentIndex-- }
            Keys.onReturnPressed: {
                if (currentItem.myData.identifier === "exit")
                    exited();
                else {
                    settingLoader.source = currentItem.myData.qml;
                }
            }

            delegate: Item {
                property variant myData: model
                id: item1
                width: 800
                height: 70
                Text {
                    text: model.name
                    anchors.horizontalCenterOffset: 10
                    anchors.top: parent.top
                    anchors.topMargin: 6
                    anchors.horizontalCenter: parent.horizontalCenter
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    font.family: "Verdana"
                    font.pointSize: 30
                    antialiasing: true
                }

                Rectangle {
                    y: 68
                    width: 780
                    height: 2
                    radius: 5
                    anchors.leftMargin: 10
                    anchors.left: parent.left
                    color: "#757add"
                }
            }

            highlight: Item {

                RectangularGlow {
                    id: effect
                    width: 800
                    height: 68
                    glowRadius: 30
                    spread: 0.0
                    color: "#393380"
                    cornerRadius: 25
                }
                Rectangle {
                    id: jura
                    y: 0
                    x: 0
                    width: 800
                    height: 68
                    radius: 10
                    gradient: Gradient {
                        GradientStop {
                            position: 0.641
                            color: "#00000000"
                        }

                        GradientStop {
                            position: 0.149
                            color: "#c2c2c2"
                        }

                        GradientStop {
                            position: 0.45
                            color: "#ffffff"
                        }

                    }
                    border.color: "white"
                    clip: false
                    border.width: 2
                    opacity: 0.4
                    smooth: true
                    antialiasing: true
                }
            }
        }
    }
}
