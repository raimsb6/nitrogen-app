import QtQuick 2.0

Item {
    anchors.fill: parent

    Image {
        anchors.centerIn: parent
        source: "qrc:/images/images/WaitNote.png"
        NumberAnimation on rotation {
            loops: Animation.Infinite
            from: 0
            to: 360
            duration: 1500 // Define the desired rotation speed.
        }
    }
}
