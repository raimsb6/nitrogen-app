import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {
    property alias text: textItem.text
    property alias state: buttonRect.state
    signal pressed()
    id: buttonContainer
    width: 200
    height: 80

    Keys.onReturnPressed: {
        if (buttonRect.state == "selected") {
            //buttonRect.state = "pressed";
            pressed();
        }
    }

    RectangularGlow {
        id: effect
        anchors.fill: parent
        glowRadius: 5
        spread: 0
        color: "#5555ff"
        cornerRadius: 15
        visible: false
    }

    Rectangle {
        id: buttonRect
        anchors.centerIn: parent
        width: parent.width - 10
        height: parent.height - 10
        radius: 10
        opacity: 0.9
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#414141" }
            GradientStop { position: 1.0; color: "#313131" }
        }
        border.color: "white"
        border.width: 1

        states: [
            State {
                name: "disabled"
                PropertyChanges { target: buttonRect; border.color: "#212121" }
                PropertyChanges { target: textItem; color: "#212121"}
                PropertyChanges { target: effect; visible: "false" }
            },
            State {
                name: "normal"
                PropertyChanges { target: buttonRect; border.color: "white" }
                PropertyChanges { target: textItem; color: "white" }
                PropertyChanges { target: effect; visible: "false" }
                PropertyChanges { target: buttonContainer; focus: "false" }
            },
            State {
                name: "selected"
                PropertyChanges { target: buttonRect; border.color: "blue" }
                PropertyChanges { target: effect; visible: "true" }
                PropertyChanges { target: buttonContainer; focus: "true" }
            },
            State {
                name: "pressed"
                PropertyChanges { target: buttonRect; border.color: "red" }
                PropertyChanges { target: effect; visible: "false" }
            }
        ]

        Text {
            id: textItem
            anchors.centerIn: parent
            color: "white"
            font.pixelSize: 40
            font.family: "Verdana"
        }
    }
}
