import QtQuick 2.0

Rectangle {
    property alias text: label.text
    id: menuItem
    x: 200
    y: 40
    color: "black"
    opacity: 0.8

    Text {
        id: label
        anchors.centerIn: parent
        font.pixelSize: 25
        font.family: 'Tahoma'
        color: "white"
    }
}
