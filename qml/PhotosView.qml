import QtQuick 2.0
import QtQuick.Layouts 1.0
import AdvancedImage 1.0
//import MyLibrary 1.0

Item {
    signal exited
    property string folderPath
    id: listContainer
    width: 1920
    height: 1030

    onFolderPathChanged: { list.setPath(folderPath); }

    function exitToMain() {
        btn1.state = "active"
        btn1.state = "pressed"
        btn1.state = "normal"
    }

    Keys.onEscapePressed: {
        exitToMain();
    }

    Keys.onDigit1Pressed: {
        exitToMain();
    }

    Keys.onDigit2Pressed: {
        btn2.state = "active"
        btn2.state = "pressed"
        btn2.state = "normal"
    }

    Keys.onDigit3Pressed: {
        if (radioGroup1.selectedIndex == 5)
            radioGroup1.selectedIndex = 0;
        else
            radioGroup1.selectedIndex++;
    }

    ColumnLayout {
        x: 60
        y: 120
        width: 320
        spacing: 30

        ButtonSmall {
            id: btn1
            height: 50
            text: " Tagasi kausta"
            color: "red"
            onPressed: exited()
        }

        ButtonSmall {
            id: btn2
            height: 50
            text: " Slaidesitus"
            color: "lightgreen"
            onPressed: {
                list.selectedUrl = list.currentItem.myData.url
                photo.visible = true;
                photo.focus = true;
                slideshow_timer.running = true;
            }
        }
    }

    GridView {
        id:list
        property string selectedUrl
        model: ImageModelTest
        width: 1400
        x: 450
        y: 30
        height: 1020
        clip: true
        cellWidth: 350
        cellHeight: 235
        highlight: Rectangle {
            id: frame
            width: list.currentItem.width
            height: list.currentItem.height
            radius: 4
            color: "transparent"
            border.color: "red"
            border.width: 4
        }
        highlightFollowsCurrentItem: true
        highlightMoveDuration: 50
        focus: true
        currentIndex: 0

        function setPath(path) {
            model.setPath(path);
            list.currentIndex = 0;
            selectedUrl = currentItem.myData.url;
            photo.source = list.selectedUrl;
        }

        Connections {
            target: ImageModelTest
            onPathChanged: { list.currentIndex = 0; }

        }

        Keys.onReturnPressed: {
            list.selectedUrl = list.currentItem.myData.url
            photo.visible = true;
            photo.focus = true;
        }
        onCurrentIndexChanged: { selectedUrl = currentItem.myData.url; photo.source = list.selectedUrl; }

        header:
            Text {
                text: list.model.title
                color: "white"
                font.pointSize: 40
                font.family: "Verdana"
                anchors.leftMargin: 10
                height: 80
            }

        delegate:
            Item {
                id: delegateItem
                property variant myData: model
                width: thumbnail.width + 8
                height: thumbnail.height + 8
                anchors.leftMargin: 4
                anchors.topMargin: 4

                Image {
                    id: thumbnail
                    source: thumb
                    asynchronous: true
                    anchors.centerIn: delegateItem
                    opacity: 1.0
                    fillMode: Image.PreserveAspectFit

                    Text {
                        id: thumbnailName
                        text: model.name;
                        color: "lightgray"
                        font.pointSize: 14
                        font.family: "OpenSans"
                        anchors.top: thumbnail.bottom
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.topMargin: 2
                    }

                    MouseArea {
                        anchors.fill: parent
                        hoverEnabled: true
                        onReleased: { list.currentIndex = index; list.selectedUrl = url; photo.visible = true; }
                    }
                }
            }
        }

    Rectangle {
        width: 300
        height: 250
        x: 56
        y: 350
        color: "black"
        opacity: 0.5
        radius: 5
        border.color: "white"
        border.width: 2
        ColumnLayout {
            width: parent.width - 30
            height: parent.height - 30
            anchors.centerIn: parent
            spacing: 15

            Text {
                color: "white"
                font.pixelSize: 20
                font.family: 'OpenSans'
                text: "Nimi: " + list.currentItem.myData.name
            }

            Text {
                color: "white"
                font.pixelSize: 20
                font.family: 'OpenSans'
                text: "Suurus: " + list.currentItem.myData.widthPixels + "x" + list.currentItem.myData.heightPixels
            }

            ColumnLayout {
                spacing: 10
                Text {
                    color: "white"
                    font.pixelSize: 20
                    font.family: 'OpenSans'
                    text: "Viimati muudetud:"

                }
                Text {
                    color: "white"
                    font.pixelSize: 20
                    font.family: 'OpenSans'
                    x: parent.x
                    y: parent.y + 10
                    text: Qt.formatDateTime(list.currentItem.myData.modified, "dd.MM.yyyy hh:mm:ss")
                }
            }

            ColumnLayout {
                Text {
                    color: "white"
                    font.pixelSize: 20
                    font.family: 'OpenSans'
                    text: "Pilt tehtud:"
                }

                Text {
                    color: "white"
                    font.pixelSize: 20
                    font.family: 'OpenSans'
                    x: parent.x
                    y: parent.y + 10
                    text: Qt.formatDateTime(list.currentItem.myData.exifTaken, "dd.MM.yyyy hh:mm:ss")
                }
            }
        }
    }

    RadioGroup {
        id: radioGroup1
        selectedIndex: GlobalSettings.lastUsedSortOrder


        onSelectedIndexChanged: {

            if (selectedIndex == 0) {
                list.model.sortRole = 257;
                list.model.sortOrder = 0;
            }
            else if (selectedIndex == 1) {
                list.model.sortRole = 257;
                list.model.sortOrder = 1;
            }
            else if (selectedIndex == 2) {
                list.model.sortRole = 260;
                list.model.sortOrder = 0;
            }
            else if (selectedIndex == 3) {
                list.model.sortRole = 260;
                list.model.sortOrder = 1;
            }
            else if (selectedIndex == 4) {
                list.model.sortRole = 261;
                list.model.sortOrder = 0;
            }
            else if (selectedIndex == 5) {
                list.model.sortRole = 261;
                list.model.sortOrder = 1;
            }
            GlobalSettings.setLastUsedSortOrder(selectedIndex);
        }
    }

    ColumnLayout {
        x: 70
        y: 650
        width: 300
        height: 30
        spacing: 20
        RowLayout {
            spacing: 15
            Rectangle {
                id: remotecolor
                width: 20
                height: 8
                color: "yellow"
                anchors.verticalCenter: parent.verticalCenter
            }
            Text {
                text: "Sorteerimine:"
                color: "white"
                font.pixelSize: 25
                font.family: 'Verdana'
            }
        }

        ButtonRadio {
            radioIndex: 0
            width: parent.width
            height: parent.height
            anchors { left: parent.left; }
            text: "Tähestiku järgi"
            radioGroup: radioGroup1
        }
        ButtonRadio {
            radioIndex: 1
            width: parent.width
            height: parent.height
            anchors { left: parent.left; right: parent.right }
            text: "Tagurpidi tähestiku järgi"
            radioGroup: radioGroup1
        }
        ButtonRadio {
            radioIndex: 2
            width: parent.width
            height: parent.height
            anchors { left: parent.left; right: parent.right }
            text: "Viimati muudetud lõpus"
            radioGroup: radioGroup1
        }
        ButtonRadio {
            radioIndex: 3
            width: parent.width
            height: parent.height
            anchors { left: parent.left; right: parent.right }
            text: "Viimati muudetud ees"
            radioGroup: radioGroup1
        }
        ButtonRadio {
            radioIndex: 4
            width: parent.width
            height: parent.height
            anchors { left: parent.left; right: parent.right }
            text: "Viimati tehtud lõpus"
            radioGroup: radioGroup1
        }
        ButtonRadio {
            radioIndex: 5
            width: parent.width
            height: parent.height
            anchors { left: parent.left; right: parent.right }
            text: "Viimati tehtud ees"
            radioGroup: radioGroup1
        }
    }

    ColumnLayout {
        visible: false
        x: 30
        y: 1000
        spacing: 20
        Text {
            id: labelname
            color: "white"
            text: "Nimi:"
            font.pointSize: 12
            font.family: 'Tahoma'

        }
        Text {
            id: lalbeldate
            color: "white"
            text: "Kuupäev:"
            font.pointSize: 12
            font.family: 'Tahoma'
        }
    }

    Item {
        id: photo
        property alias source: fullimage.url
        visible: false
        x: 0
        y: 0
        width: 1980
        height: 1080

        onVisibleChanged: {
            fullimage.visible = visible;
        }

        Rectangle {
            width: parent.width
            height: parent.height
            color: "black"
        }

        Timer {
            id: slideshow_timer
            interval: GlobalSettings.slideshowTime * 1000
            repeat: true
            onTriggered: {
                if (list.currentIndex == list.count - 1) {
                    running = false;
                    photo.visible = false;
                    list.focus = true;
                }
                else
                    list.currentIndex++;
            }
        }

        Fullimage {
            id: fullimage
            anchors.fill: parent
            property string url
            onUrlChanged: {
                activeUrl = url;
                if (list.count > 0) {
                    if (list.currentIndex == list.count - 1)
                        console.log("next: none");
                    else {
                        preloadUrl = list.model.getNextUrl(list.currentIndex + 1);
                    }
                }
            }

        }

        Keys.onReturnPressed: { slideshow_timer.running = false; photo.visible = false; photo.focus = false; list.focus = true }
        Keys.onRightPressed: { if (list.currentIndex < (list.count - 1)) list.currentIndex++ }
        Keys.onLeftPressed: { if (list.currentIndex > 0) list.currentIndex-- }
        Keys.onEscapePressed: { slideshow_timer.running = false; photo.visible = false; photo.focus = false; list.focus = true }

        MouseArea {
            anchors.fill: parent
            onReleased: { photo.visible = false; photo.focus = false; list.focus = true }
            onWheel: {
                if (wheel.angleDelta.y < 0) {
                    list.currentIndex++;
                }
                else
                    list.currentIndex--;
            }
        }
    }
}
