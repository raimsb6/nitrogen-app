import QtQuick 2.0
import QtQuick.Layouts 1.0
import AdvancedImage 1.0

Item {
    property string activeUrl
    property string preloadUrl
    property int activeIndex
    property bool image1Active
    property bool image2Active
    property bool preloadChangedDuringAnimation

    Component.onCompleted: {
        activeIndex = 0;
        image1Active = false;
        image2Active = false;
        image1.opacity = 0;
        image2.opacity = 0;
        preloadChangedDuringAnimation = false;
    }

    function loadPreload() {
        preloadChangedDuringAnimation = false;
        if (activeIndex == 0) {
            console.log("preload: image2 " + preloadUrl);
            image2.source = "file:///" + preloadUrl;
        }
        else {
            console.log("preload: image1 " + preloadUrl);
            image1.source = "file:///" + preloadUrl;
        }
    }

    onPreloadUrlChanged: {
        console.log("onPreloadUrlChanged");
        if (fadein1.running || fadein2.running)
            preloadChangedDuringAnimation = true;
        else
            loadPreload();
    }

    onActiveUrlChanged: {
        // 1) fade1 in 2) fade2 in 3) fade1 in fade2 out 4) fade2 in fade1 out
        var usecase;
        console.log("onActiveUrlChanged");
        if (activeIndex == 0) {
            if (image1Active == false) {
                image1Active = true;
                usecase = 1;
            }
            else {
                image1Active = false;
                image2Active = true;
                activeIndex = 1;
                usecase = 4;
            }
        }
        else if (activeIndex == 1) {
            if (image2Active == false) {
                image2Active = true;
                usecase = 2;
            }
            else {
                image1Active = true;
                image2Active = false;
                activeIndex = 0;
                usecase = 3;
            }
        }
        console.log("use case: " + usecase);
        if (usecase === 1 || usecase === 3) {
            if (preloadUrl != activeUrl) {
                console.log("active: image1 " + activeUrl);
                image1.source = "file:///" + activeUrl;
            }
            console.log("showing image1");
            image1.opacity = 0;
            image1.z = 100;
            image2.z = 99;
            fadein1.start();
        }
        else if (usecase === 2 || usecase === 4) {
            if (preloadUrl != activeUrl) {
                console.log("active: image2 " + activeUrl);
                image2.source = "file:///" + activeUrl;
            }
            console.log("showing image2");
            image2.opacity = 0;
            image2.z = 100;
            image1.z = 99;
            fadein2.start();
        }
    }

    Rectangle {
        property alias source: innerImage1.source
        id: image1
        anchors.fill: parent
        color: "black"

        NumberAnimation {
            id: fadein1
            target: image1;
            property: "opacity";
            from: 0;
            to: 1;

            onStopped: {
                if (preloadChangedDuringAnimation)
                    loadPreload();
            }
        }


        Image {
            id: innerImage1
            width: parent.width
            height: parent.height
            fillMode: Image.PreserveAspectFit
            asynchronous: true
            sourceSize.width: 1980
            sourceSize.height: 1080
            anchors.fill: parent


        }
    }

    Rectangle {
        property alias source: innerImage2.source
        id: image2
        anchors.fill: parent
        color: "black"

        NumberAnimation {
            id: fadein2
            target: image2;
            property: "opacity";
            from: 0;
            to: 1;

            onStopped: {
                if (preloadChangedDuringAnimation)
                    loadPreload();
            }
        }

        Image {
            id: innerImage2
            width: parent.width
            height: parent.height
            fillMode: Image.PreserveAspectFit
            asynchronous: true
            sourceSize.width: 1980
            sourceSize.height: 1080
            anchors.fill: parent


        }
    }
}
