import QtQuick 2.0
import QtQuick.Layouts 1.0

Rectangle {
    id: main
    width: 1920
    height: 1080
    
    color: "black"
    focus: true

    Image {
        source: "qrc:/images/images/background.jpg"
        anchors.fill: parent
    }

    Notification {
        id: notification
        objectName: "notification"
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: 20
    }

    Keys.onDigit1Pressed: { if (GlobalSettings.stickAttached) { pageLoader.folderPath = "/media/sda1/"; pageLoader.source = "FolderView.qml" } }
    Keys.onDigit2Pressed: { pageLoader.folderPath = "/meedia/"; pageLoader.source = "FolderView.qml" }
    Keys.onDigit3Pressed: { pageLoader.source = "SettingsView.qml" }
    Keys.onEscapePressed: { Qt.quit() }

    MenuButtons {
        id: mainmenu
        button1.onPressed: { pageLoader.folderPath = "/media/"; pageLoader.source = "FolderView.qml" }
        button2.onPressed: { pageLoader.folderPath = "/meedia/"; pageLoader.source = "FolderView.qml" }
        button4.onPressed: { pageLoader.source = "SettingsView.qml" }
    }

    Loader {
        id: pageLoader
        property string folderPath
        onLoaded: {
            item.toplevel = folderPath;
            item.visible = true;
            mainmenu.visible = false;
            focus = true
        }

        Connections {
            id: menuConnection
            target: pageLoader.item
            ignoreUnknownSignals: true
            onExited:
            {
                mainmenu.visible = true;
                pageLoader.item.visible = false;
                pageLoader.source = "";
                mainmenu.focus = true;
                if (GlobalSettings.stickAttached)
                {
                    mainmenu.button1.visible = true;
                    mainmenu.button1.state = "normal";
                }
                else
                {
                    mainmenu.button1.visible = false;
                    mainmenu.button1.state = "hidden";
                }
                mainmenu.button2.state = "normal";
                mainmenu.button4.state = "normal";
            }
        }
    }

    Connections {
        target: GlobalSettings
        onStickAttachedChanged: {
            if (target.stickAttached)
                mainmenu.button1.state = "normal";
            else
            {
                mainmenu.button1.state = "hidden";
                mainmenu.button1.visible = false;
            }
        }
    }

    Rectangle {
        id: darkenbox
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
        color: "transparent"
        opacity: 0.5
        visible: false
    }
}
