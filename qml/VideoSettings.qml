import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {
    signal exited
    property string folderPath
    property string toplevel

    anchors.fill: parent

    Item {
        id: content

        Text {
            x: 500
            y: 250
            text: "Video seaded"
            font.family: "Verdana"
            font.pointSize: 50
            color: "#757add"
        }

        ListModel {
            id: statusMenuModel

            function value(identifier) {
                    return "?";
            }
        }
        Timer {
            interval: 1000
            repeat: true
            running: true
            onTriggered: {
                GlobalSettings.refresh();
            }
        }

        ListView {
            id: menuList
            x: 550
            y: 400
            width: 400
            height: 500
            model: statusMenuModel
            focus: true
            currentIndex: 0
            highlightFollowsCurrentItem: true
            highlightMoveDuration: 50

            Keys.onEscapePressed: {
                console.log("status exit");
                exited();
            }
            Keys.onReturnPressed: {
            }

            delegate: Item {
                property variant myData: model
                id: item1
                width: 800
                height: 70
                Text {
                    text: model.name
                    anchors.left: parent.left
                    anchors.leftMargin: 20
                    anchors.top: parent.top
                    anchors.topMargin: 6
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    font.family: "Verdana"
                    font.pointSize: 30
                    antialiasing: true
                }

                Text {
                    text: statusMenuModel.value(model.identifier)
                    anchors.top: parent.top
                    anchors.topMargin: 6
                    anchors.right: parent.right
                    anchors.rightMargin: 20

                    horizontalAlignment: Text.AlignRight
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    font.family: "Verdana"
                    font.pointSize: 30
                    antialiasing: true
                }

                Rectangle {
                    y: 68
                    width: 780
                    height: 2
                    radius: 5
                    anchors.leftMargin: 10
                    anchors.left: parent.left
                    color: "#757add"
                }
            }
            highlight: Item {

                RectangularGlow {
                    id: effect
                    width: 800
                    height: 68
                    glowRadius: 30
                    spread: 0.0
                    color: "#393380"
                    cornerRadius: 25
                }
                Rectangle {
                    id: jura
                    y: 0
                    x: 0
                    width: 800
                    height: 68
                    radius: 10
                    gradient: Gradient {
                        GradientStop {
                            position: 0.641
                            color: "#00000000"
                        }

                        GradientStop {
                            position: 0.149
                            color: "#c2c2c2"
                        }

                        GradientStop {
                            position: 0.45
                            color: "#ffffff"
                        }

                    }
                    border.color: "white"
                    clip: false
                    border.width: 2
                    opacity: 0.4
                    smooth: true
                    antialiasing: true
                }
            }
        }
    }
}
