import QtQuick 2.0

Flow {
    property alias button1: btn1
    property alias button2: btn2
    //property alias button3: btn3
    property alias button4: btn4
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.verticalCenter: parent.verticalCenter
    spacing: 50
    
    Button {
        id: btn1
        text: "Mälupulk"
        image: "qrc:/images/images/usb.png"
        color: "red"
    }
    
    Button {
        id: btn2
        text: "Multimeedia"
        image: "qrc:/images/images/matrix.png"
        color: "green"
    }
    
    /*Button {
        id: btn3
        text: "Videod"
        image: "qrc:/images/images/video.png"
        KeyNavigation.right: btn3
        KeyNavigation.left: btn1
    }*/
    
    Button {
        id: btn4
        text: "Seaded"
        image: "qrc:/images/images/settings.png"
        color: "yellow"
    }
}
