import QtQuick 2.0
import QtQuick.Layouts 1.0
import QtMultimedia 5.0
import GVideoPlayer 1.0

Item {
    signal exited
    property string folderPath
    id: videoView
    width: 1920
    height: 1080

    onVisibleChanged: {
        if (videoView.visible == true)
            console.log("videoView visible, currentindex" + list.currentIndex);
            //list.currentIndex = -1;
            //list.setPath(folderPath)
    }

    onFolderPathChanged: { list.setPath(folderPath); }

    Keys.onDigit1Pressed: {
        video.stop();
        btn1.state = "active"
        btn1.state = "pressed"
        btn1.state = "normal"
    }
    Keys.onReturnPressed: {
        video.stop();
        fullBackground.visible = true;
        video.x = fullBackground.x;
        video.y = fullBackground.y;
        video.width = fullBackground.width;
        video.height = fullBackground.height;
        video.fullscreen = true;
        video.play();
    }
    Keys.onEscapePressed: {
        video.stop();
        video.x = videorectangle.x;
        video.y = videorectangle.y;
        video.width = videorectangle.width;
        video.height = videorectangle.height;
        fullBackground.visible = false;
        video.fullscreen = false;
        video.play();
    }

    ColumnLayout {
        x: 60
        y: 120
        width: 320
        spacing: 30

        ButtonSmall {
            id: btn1
            height: 50
            text: " Tagasi kausta"
            color: "red"
            onPressed: exited()
        }
    }
    Rectangle {
        x: 1100
        y: 380
        width: 650
        height: 360
        color: "black"
        opacity: 0.5
        radius: 5
        border.color: "white"
        border.width: 2

        ColumnLayout {
            width: parent.width - 30
            height: parent.height - 30
            anchors.centerIn: parent
            spacing: 20

            Text {
                color: "white"
                text: "Nimi: " + list.currentItem.myData.name
                font.pixelSize: 20
                font.family: 'Tahoma'
            }
            Text {
                color: "white"
                text: "Pikkus: " + list.currentItem.myData.lengthSec
                font.pixelSize: 20
                font.family: 'Tahoma'
            }
            Text {
                color: "white"
                text: "Suurus: " + list.currentItem.myData.widthPixels + "x" + list.currentItem.myData.heightPixels
                font.pixelSize: 20
                font.family: 'Tahoma'
            }
            Text {
                color: "white"
                text: "Konteineri formaat: " + list.currentItem.myData.container
                font.pixelSize: 20
                font.family: 'Tahoma'
            }
            Text {
                color: "white"
                text: "Video kodeering: " + list.currentItem.myData.videoCodec
                font.pixelSize: 20
                font.family: 'Tahoma'
            }
            Text {
                color: "white"
                text: "Audio kodeering: " + list.currentItem.myData.audioCodec
                font.pixelSize: 20
                font.family: 'Tahoma'
            }
            Text {
                color: "white"
                text: "Bitrate: " + list.currentItem.myData.bitrate + " MB/s"
                font.pixelSize: 20
                font.family: 'Tahoma'
            }
            Text {
                color: "white"
                text: "Kaadreid sekundis: " + list.currentItem.myData.framerate;
                font.pixelSize: 20
                font.family: 'Tahoma'
            }
        }
    }

    ListView {
        id: list
        property string selectedUrl
        model: VideoModel
        width: 1200
        x: 450
        y: 30
        height: 1050
        clip: true
        highlight: Rectangle {
            id: frame
            width: 608
            height: 358
            radius: 4
            color: "transparent"
            border.color: "red"
            border.width: 4
            anchors.leftMargin: 50
        }
        highlightFollowsCurrentItem: true
        highlightMoveDuration: 100
        focus: true
        currentIndex: 0
        highlightRangeMode: "StrictlyEnforceRange"
        preferredHighlightBegin: 350
        preferredHighlightEnd: 650
        spacing: 30

        function setPath(path) {
            model.setPath(path);
            video.uri = list.currentItem.myData.url;
            list.currentIndex = 0;
            video.play();
        }

        onCurrentIndexChanged: {
            video.stop();
            video.uri = list.currentItem.myData.url;
            video.play();
        }

        header:
            Text {
                text: list.model.title
                color: "white"
                font.pointSize: 40
                font.family: "Tahoma"
                anchors.leftMargin: 10
                height: 80
            }

        delegate:
            Item {
                id: delegateItem
                property variant myData: model
                width: 408
                height: 358
                anchors.leftMargin: 4
                anchors.topMargin: 4

                Rectangle {
                    id: thumbnail
                    anchors.centerIn: delegateItem
                    opacity: 1.0

                    Image {
                        source: "qrc:/images/images/video.png"
                        anchors.centerIn: parent
                    }

                    Text {
                        text: model.name
                        color: "white"
                        anchors.horizontalCenter: parent.verticalCenter
                        x: parent.x + parent.width
                        font.pixelSize: 20
                        font.family: 'Tahoma'
                    }

                    MouseArea {
                        anchors.fill: parent
                        hoverEnabled: true
                        //onReleased: { list.currentIndex = index; list.selectedUrl = url; photo.visible = true; }
                        onClicked: { video.play(); }
                    }
                }
            }
    }

    Rectangle {
        id: videorectangle
        width: 600
        height: 350
        x: 454
        y: 384
        color: "black"

        GVideoPlayer {
            id: video
            x: parent.x;
            y: parent.y;
            width: parent.width;
            height: parent.height;
        }
    }

    Rectangle {
        id: fullBackground
        x: 0
        y: 0
        width: 1920
        height: 1080
        color: "black"
        visible: false
    }
}
