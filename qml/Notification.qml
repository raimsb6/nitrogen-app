import QtQuick 2.0

Rectangle {
    id: notification
    property alias text: label.text

    width: label.paintedWidth + 20;
    height: 40
    color: "lightgray"
    radius: 5
    opacity: 1.0
    state: "hidden"

    gradient: Gradient {
            GradientStop { position: 0.0; color: "lightgray" }
            GradientStop { position: 1.0; color: "gray" }
        }

    onTextChanged: { state = "shown" }

    Text {
        id: label
        color: "black"
        anchors.centerIn: parent
        font.pointSize: 18
        font.bold: false
    }

    Timer {
        id: timer
        interval: 5000
        running: false
        repeat: false
        onTriggered: parent.state = "hidden"
    }

    states: [
        State {
            name: "shown"
            PropertyChanges { target: notification; opacity: 1.0 }
            PropertyChanges { target: timer; running: true }
        },
        State {
            name: "hidden"
            PropertyChanges { target: notification; opacity: 0.0 }
        }
    ]

    transitions: [
        Transition {
            from: "shown"; to: "hidden"
            PropertyAnimation { property: "opacity"; to: 0.0; duration: 1000 }
        },
        Transition {
            from: "hidden"; to: "shown"
            PropertyAnimation { property: "opacity"; to: 1.0; duration: 300 }
        }
    ]
}
