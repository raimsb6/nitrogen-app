import QtQuick 2.0
import FileCopyHandler 1.0

Item {
    property int percentProgress: 0
    property int totalFiles: 0
    property int currentFiles: 0
    property string filename: ""
    property string label: "Valmistan kopeerimist ette..."
    signal closed()

    function copyToExt(path) {
        progress.animationEnabled = false;
        percentProgress = 0;
        progress.animationEnabled = true;
        copyHandler.copyDirToExternalMemoryStart(path);
        closeButton.state = "disabled";
        copyMessageBox.visible = false;
    }

    function copyToInt(path) {
        progress.animationEnabled = false;
        percentProgress = 0;
        progress.animationEnabled = true;
        copyHandler.copyDirFromExternalMemoryStart(path);
        closeButton.state = "disabled";
        copyMessageBox.visible = false;
    }

    FileCopyHandler {
        id: copyHandler
        onCopyMetrics: {
            totalFiles = numFiles;
            label = "Kopeerin..."
        }
        onCopyDone: {
            console.log("copy done with status " + copyStatus);
            filename = "";
            closeButton.state = "selected";
            if (copyStatus === FileCopyHandler.RESULT_OK) {
                label = "Kopeeritud";
            }
            else {
                label = "Kopeerimine ebaõnnestus";
                if (copyStatus === FileCopyHandler.DIR_ALREADY_EXISTS) {
                    copyMessageBox.text = "See kaust on juba olemas";
                    copyMessageBox.visible = true;
                }
                else if (copyStatus === FileCopyHandler.MEMORY_STICK_NOT_ATTACHED) {
                    copyMessageBox.text = "Mälupulk pole küljes";
                    copyMessageBox.visible = true;
                }
                else if (copyStatus === FileCopyHandler.NOT_ENOUGH_FREE_SPACE) {
                    copyMessageBox.text = "Pole piisavalt vaba ruumi";
                    copyMessageBox.visible = true;
                }
                else if (copyStatus === FileCopyHandler.FILE_AREADY_EXISTS) {
                    copyMessageBox.text = "Fail on juba olemas";
                    copyMessageBox.visible = true;
                }
            }
        }
        onCopyProgress: {
            currentFiles = filesCopied;
            percentProgress = percent;
            filename = currentFile;
        }


    }

    Dialog {
        width: 1200
        height: 700
        title: "Kopeerimine"

        Text {
            x: 30
            y: 80
            text: label
            color: "white"
            font.pixelSize: 70
        }

        Text {
            id: filenameText
            x: 40
            y: 250
            width: parent.width - 80
            elide: Text.ElideRight
            maximumLineCount: 1
            text: filename + " (" + currentFiles +" / " + totalFiles + ")";
            color: "white"
            font.pixelSize: 40
        }

        SimpleProgressBar {
            id: progress
            width: 1120
            height: 40
            anchors.horizontalCenter: parent.horizontalCenter
            y: 350
            color: "white"
            value: percentProgress
        }

        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            y: 400
            text: percentProgress + " %"

            color: "white"
            font.pixelSize: 40
        }

        DialogButton {
            id: closeButton
            anchors.horizontalCenter: parent.horizontalCenter
            y: 550
            text: "OK"

            onPressed: {
                console.log("OK button pressed");
                closed();
            }
        }
    }

    MessageBox {
        id: copyMessageBox
        visible: false
        onClosed: {
            console.log("messagebox close event");
            visible = false;
            closeButton.focus = true;
        }
    }
}
