import QtQuick 2.0

Item {
    signal exited
    property string folderPath
    property string toplevel

    anchors.fill: parent

    Item {
        id: content

        Text {
            x: 500
            y: 250
            text: "Süsteemi olek"
            font.family: "Verdana"
            font.pointSize: 50
            color: "#757add"
        }

        ListModel {
            id: statusMenuModel
            ListElement {
                identifier: "id1"
                name: "Võrguühendus"
            }
            ListElement {
                identifier: "id2"
                name: "IP aadress"
            }
            ListElement {
                identifier: "id3"
                name: "Netmask"
            }
            ListElement {
                identifier: "id4"
                name: "Gateway"
            }
            ListElement {
                identifier: "id5"
                name: "MAC"
            }
            ListElement {
                identifier: "id6"
                name: "HDMI CEC ühendatud"
            }
            ListElement {
                identifier: "id7"
                name: "Android ühendus"
            }
            ListElement {
                identifier: "id8"
                name: "Mälupulk ühendatud"
            }
            function value(identifier) {
                if (identifier === "id8")
                    return GlobalSettings.stickAttached ? "Jah" : "Ei";
                else if (identifier === "id1")
                    return GlobalSettings.networkUp ? "Jah" : "Ei";
                else if (identifier === "id2")
                    return GlobalSettings.ipAddress;
                else if (identifier === "id3")
                    return GlobalSettings.netmask;
                else if (identifier === "id4")
                    return GlobalSettings.gateway;
                else if (identifier === "id5")
                    return GlobalSettings.macAddress;
                else if (identifier === "id6")
                    return GlobalSettings.hdmiConnected ? "Jah" : "Ei";
                else if (identifier === "id7")
                    return GlobalSettings.androidConnected ? "Jah" : "Ei";
                else
                    return "?";
            }
        }
        Timer {
            interval: 1000
            repeat: true
            running: true
            onTriggered: {
                GlobalSettings.refresh();
            }
        }

        ListView {
            id: menuList
            x: 550
            y: 400
            width: 400
            height: 500
            model: statusMenuModel
            focus: true
            currentIndex: 0

            Keys.onEscapePressed: {
                console.log("status exit");
                exited();
            }

            delegate: Item {
                property variant myData: model
                id: item1
                width: 800
                height: 70
                Text {
                    text: model.name
                    anchors.left: parent.left
                    anchors.leftMargin: 20
                    anchors.top: parent.top
                    anchors.topMargin: 6
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    font.family: "Verdana"
                    font.pointSize: 30
                    antialiasing: true
                }

                Text {
                    text: statusMenuModel.value(model.identifier)
                    anchors.top: parent.top
                    anchors.topMargin: 6
                    anchors.right: parent.right
                    anchors.rightMargin: 20

                    horizontalAlignment: Text.AlignRight
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    font.family: "Verdana"
                    font.pointSize: 30
                    antialiasing: true
                }

                Rectangle {
                    y: 68
                    width: 780
                    height: 2
                    radius: 5
                    anchors.leftMargin: 10
                    anchors.left: parent.left
                    color: "#757add"
                }
            }
        }
    }
}
