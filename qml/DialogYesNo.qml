import QtQuick 2.0
import QtQuick.Layouts 1.0

Dialog {
    property alias text: tekst.text
    signal closed(int reason)
    id: dialog
    title: "Küsimus"
    width: 800
    height: 500

    Text {
        id: tekst
        y: 120
        x: 50
        width: parent.width - 100
        height: 200
        color: "white"
        wrapMode: Text.WordWrap
        text: ""
        font.pixelSize: 40
    }

    Keys.onRightPressed: {
        if (yesButton.state == "selected") {
            yesButton.state = "normal";
            noButton.state = "selected";
        }
    }
    Keys.onLeftPressed: {
        if (noButton.state == "selected") {
            noButton.state = "normal";
            yesButton.state = "selected";
        }
    }
    Keys.onEscapePressed: {
        closed(0);
    }
    onVisibleChanged: {
        if (visible) {
            yesButton.state = "normal";
            noButton.state = "selected";
        }
    }
    onFocusChanged: {
        if (noButton.state = "selected")
            noButton.focus = true;
        else
            yesButton.focus = true;
    }

    RowLayout {
        y: 350
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 40

        DialogButton {
            id: yesButton
            text: "JAH"
            state: "normal"

            onPressed: {
                closed(1);
            }
        }

        DialogButton {
            id: noButton
            text: "EI"
            state: "selected"

            onPressed: {
                closed(0);
            }
        }
    }
}
