import QtQuick 2.0
import QtQuick.Layouts 1.0

Item {
    signal exited
    property string folderPath
    property string toplevel

    Loader {
        id: photoLoader
        onLoaded: { item.visible = true; focus = true }
    }

    onToplevelChanged: { list.model.topFolder = toplevel; folderPath = toplevel }
    Connections {
            id: menuConnection
            target: photoLoader.item
            ignoreUnknownSignals: true
            onExited:
            {
                //photoLoader.destroy();
                photoLoader.visible = false;
                photoLoader.source = "";
                folderView.visible = true;
                list.focus = true;

            }
    }

    function exitToMain() {
        btn1.state = "active"
        btn1.state = "pressed"
        btn1.state = "normal"
    }

    onFolderPathChanged: { list.model.setPath(folderPath); }
    Keys.onDigit1Pressed: {
        exitToMain();
    }
    Keys.onEscapePressed: {
        exitToMain();
    }

    Item {
        id: folderView

    ColumnLayout {
        x: 60
        y: 120
        width: 320
        spacing: 30

        ButtonSmall {
            id: btn1
            height: 50
            text: " Tagasi peamenüüsse"
            color: "red"
            onPressed: exited()
        }
    }

    GridView {
        id:list
        property string selectedUrl
        model: FolderModel
        width: 1400
        x: 450
        y: 30
        height: 1020
        clip: true
        cellWidth: 350
        cellHeight: 235
        highlight: Rectangle {
            id: frame
            width: 300
            height: 200
            radius: 4
            color: "transparent"
            border.color: "red"
            border.width: 4
        }
        highlightFollowsCurrentItem: true
        highlightMoveDuration: 50
        focus: true
        currentIndex: 0

        onCurrentIndexChanged: { selectedUrl = currentItem.myData.url }
        Keys.onReturnPressed: {
            if (currentItem.myData.url === "photos") {
                if (model.numPhotos > 0) {
                    folderView.visible = false;
                    photoLoader.source = "PhotosView.qml"
                    photoLoader.item.folderPath = model.path;
                    photoLoader.visible = true;
                    photoLoader.focus = true;
                }
            } else if (currentItem.myData.url === "videos") {
                if (model.numVideos > 0) {
                    folderView.visible = false;
                    photoLoader.source = "VideoView.qml";
                    photoLoader.item.folderPath = model.path;
                    photoLoader.visible = true;
                    photoLoader.focus = true;
                }
            } else if (currentItem.myData.url === "upload") {
                copyProgress.visible = true;
                copyProgress.focus = true;
                copyProgress.copyToExt(model.path);
            } else if (currentItem.myData.url === "download") {
                copyProgress.visible = true;
                copyProgress.focus = true;
                copyProgress.copyToInt(model.path);
            } else if (currentItem.myData.url === "delete") {
                deleteConfirmation.text = "Oled kindel et soovid kustutada kausta '" + model.title + "' ?";
                deleteConfirmation.visible = true;
                deleteConfirmation.focus = true;
            } else {
                model.setPath(selectedUrl);
            }
        }


        Connections {
            target: FolderModel
            onPathChanged: { list.currentIndex = 0; list.selectedUrl = list.currentItem.myData.url }

        }

        Component.onCompleted: { model.setPath(folderPath); }
        //FolderModel.onPathChanged: {}

        header:
            Text {
                text: list.model.title
                color: "white"
                font.pointSize: 40
                font.family: "Verdana"
                anchors.leftMargin: 10
                height: 80
            }

        delegate:
            Item {
                id: delegateItem
                property variant myData: model
                width: 300
                height: 200
                anchors.leftMargin: 4
                anchors.topMargin: 4

                Image {
                    id: thumbnail
                    source: thumb
                    anchors.centerIn: delegateItem
                    height: 200
                    fillMode: Image.PreserveAspectFit

                    Text {
                        id: thumbnailName
                        text: {
                            if (url === "photos")
                                model.name + " (" + list.model.numPhotos + ")" ;
                            else if (url === "videos")
                                model.name + " (" + list.model.numVideos + ")";
                            else
                                model.name;
                        }

                        color: "lightgray"
                        font.pointSize: 16
                        font.family: "OpenSans"
                        anchors.top: thumbnail.bottom
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.topMargin: 5
                    }

                    MouseArea {
                        anchors.fill: parent
                        hoverEnabled: true
                        onReleased: { list.currentIndex = index; list.selectedUrl = url; photo.visible = true; }
                    }
                }
            }
        }
    }

    DialogYesNo {
        id: deleteConfirmation;
        visible: false;
        focus: false;

        onClosed: {
            visible = false;
            list.focus = true;

            if (reason == 1) {
                // exit folder and delete it recursively
                var parentPath = list.model.parentPath;
                list.model.remove(list.model.path);
                list.model.setPath(parentPath);
            }
        }
    }

    CopyProgress {
        id: copyProgress
        visible: false
        focus: false

        Component.onCompleted: list.focus = true;

        onClosed: {
            copyProgress.visible = false;
            list.focus = true;
        }
    }
}

