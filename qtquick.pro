# Add more folders to ship with the application, here
folder_01.source = qml
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

linux-*{
    target.path = /opt/qtdemo
    #INSTALLS += target
}

#CONFIG += link_pkgconfig
#PKGCONFIG += Qt5GStreamer-0.10 Qt5GStreamerUi-0.10
QMAKE_CXXFLAGS += -pthread -I/home/raimo/yocto/build/tmp/sysroots/nitrogen6x/usr/include/Qt5GStreamer -I/home/raimo/yocto/build/tmp/sysroots/nitrogen6x/usr/include/ --sysroot=/home/raimo/yocto/build/tmp/sysroots/nitrogen6x
QMAKE_LFLAGS += -L/home/raimo/yocto/build/tmp/sysroots/nitrogen6x/usr/lib --sysroot=/home/raimo/yocto/build/tmp/sysroots/nitrogen6x
QMAKE_LIBS += -lQt5GStreamer-0.10 -lQt5GStreamerQuick-0.10 -lQt5GLib-2.0 -lQt5Core -lQt5GStreamerUi-0.10 -lQt5Widgets -lQt5GStreamer-0.10 -lQt5Gui -lQt5GLib-2.0 -lQt5Core -ludev -lcec -lexif
QT += concurrent opengl network

# Additional import path used to resolve QML modules in Creator's code model
#QML_IMPORT_PATH =

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    imagelistmodel.cpp \
    imagedata.cpp \
    imagelistmodelsortproxy.cpp \
    folderlistmodel.cpp \
    folderdata.cpp \
    videolistmodel.cpp \
    videodata.cpp \
    gvideoplayer.cpp \
    udevhandler.cpp \
    tcphandler.cpp \
    cechandler.cpp \
    settings.cpp \
    mainapp.cpp \
    filecopyhandler.cpp \
    utils.cpp \
    advancedimage.cpp

# Installation path
# target.path =

# Please do not modify the following two lines. Required for deployment.
include(qtquick2applicationviewer/qtquick2applicationviewer.pri)
qtcAddDeployment()

RESOURCES += \
    resources.qrc

OTHER_FILES += \
    qml/Clock.qml \
    qml/Button.qml \
    qml/ButtonRadio.qml \
    qml/ButtonSmall.qml \
    qml/FolderView.qml \
    qml/main.qml \
    qml/MenuButtons.qml \
    qml/MenuItem.qml \
    qml/Notification.qml \
    qml/PhotosView.qml \
    qml/RadioGroup.qml \
    qml/SettingsView.qml \
    qml/VideoView.qml

HEADERS += \
    imagelistmodel.h \
    imagedata.h \
    imagelistmodelsortproxy.h \
    folderlistmodel.h \
    folderdata.h \
    videolistmodel.h \
    videodata.h \
    gvideoplayer.h \
    udevhandler.h \
    tcphandler.h \
    cechandler.h \
    settings.h \
    mainapp.h \
    filecopyhandler.h \
    utils.h \
    advancedimage.h
