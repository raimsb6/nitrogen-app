#include "imagelistmodelsortproxy.h"
#include "imagelistmodel.h"

ImageListModelSortProxy::ImageListModelSortProxy(QObject *parent) : QSortFilterProxyModel(parent)
{
    this->setDynamicSortFilter(true);
}

void ImageListModelSortProxy::setPath(const QString &path)
{
    ImageListModel *model;
    model = (ImageListModel *)this->sourceModel();
    model->setPath(path);
}

QString ImageListModelSortProxy::getPath()
{
    ImageListModel *model;
    model = (ImageListModel *)this->sourceModel();
    return model->getPath();
}

void ImageListModelSortProxy::setTitle(const QString &title)
{
    ImageListModel *model;
    model = (ImageListModel *)this->sourceModel();
    model->setTitle(title);
}

QString ImageListModelSortProxy::getTitle()
{
    ImageListModel *model;
    model = (ImageListModel *)this->sourceModel();
    return model->getTitle();
}

int ImageListModelSortProxy::columnCount(const QModelIndex&) const
{
    ImageListModel *model;
    model = (ImageListModel *)this->sourceModel();
    qDebug() << "column count" << model->roleNames().count();
    return model->roleNames().count();
}

int ImageListModelSortProxy::getSortOrder()
{
    return sortOrder();
}

void ImageListModelSortProxy::setSortOrder(int order)
{
    sort(0, (Qt::SortOrder)order);
}

QString ImageListModelSortProxy::getNextUrl(int rowNum)
{
    return data(index(rowNum, 0), ImageListModel::UrlRole).toString();
}
