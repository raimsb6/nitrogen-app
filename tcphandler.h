#ifndef TCPHANDLER_H
#define TCPHANDLER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>

class TcpHandler : public QObject
{
    Q_OBJECT
public:
    explicit TcpHandler(QObject *parent = 0);
    void Init(QObject *receiver);
    void Deinit();

private:
    QTcpServer server;
    QTcpSocket *socket;
    QObject *keyReceiver;
    bool connected;

    void handleMsg(QByteArray msg);

signals:
    void connectionOpened(QString ip);
    void connectionClosed();

public slots:
    void newConnection();
    void dataReady();
    void socketError();
    void disconnectedConnection();

};

#endif // TCPHANDLER_H
