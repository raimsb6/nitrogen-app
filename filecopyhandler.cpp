#include <QApplication>
#include <QtConcurrent>
#include <QThread>
#include <QFileInfo>
#include <QList>
#include "filecopyhandler.h"
#include "settings.h"
#include "utils.h"

FileCopyHandler::FileCopyHandler(QObject *parent) :
    QObject(parent)
{
    running = 0;
    connect(&futureWatcher, SIGNAL(finished()), this, SLOT(CopyFinished()));
}

void FileCopyHandler::copyDirToExternalMemoryStart(QString sourceDir)
{
    QDir sDir(sourceDir);

    totalBytes = 0;
    totalFiles = 0;
    copiedFiles = 0;
    copiedBytes = 0;

    future = QtConcurrent::run(this, &FileCopyHandler::internalCopyToExtMemory, (const QDir&)sDir);
    futureWatcher.setFuture(future);
}

void FileCopyHandler::copyDirFromExternalMemoryStart(QString sourceDir)
{
    QDir sDir(sourceDir);
    totalBytes = 0;
    totalFiles = 0;
    copiedFiles = 0;
    copiedBytes = 0;

    future = QtConcurrent::run(this, &FileCopyHandler::internalCopyToIntMemory, (const QDir&)sDir);
    futureWatcher.setFuture(future);
}

FileCopyHandler::FileCopyHandlerResult FileCopyHandler::internalCopyToExtMemory(QDir sourceDirectory)
{
    QList<QFileInfo> copyList;
    Settings *settings = qApp->property("settings").value<Settings *>();

    if (!sourceDirectory.exists())
        return RESULT_SOURCE_DIR_NOT_EXIST;
    if (!settings->getStickAttached())
        return MEMORY_STICK_NOT_ATTACHED;
    QDir mountDir(extMediaMountPoint);
    if (!mountDir.exists() || !mountDir.Writable)
        return MEMORY_STICK_NOT_WRITABLE;

    getMediaFilesRecursively(sourceDirectory, copyList);
    emit copyMetrics(totalFiles, totalBytes);

    quint64 freeSpace = Utils::getFreeSpace(extMediaMountPoint);
    if (freeSpace < totalBytes)
        return NOT_ENOUGH_FREE_SPACE;

    QDir destDir(extMediaMountPoint + sourceDirectory.dirName());
    if (destDir.exists())
        return DIR_ALREADY_EXISTS;

    QString destPrefix = extMediaMountPoint + sourceDirectory.dirName() + "/";
    // determine destination directories and actually copy
    foreach (QFileInfo finfo, copyList)
    {
        currentFileName = finfo.fileName();
        QString relativePart = sourceDirectory.relativeFilePath(finfo.absoluteFilePath());
        QFileInfo destination(destPrefix + relativePart);
        // make necessary directories
        QDir filedir(destination.absoluteDir());
        if (!filedir.exists())
            filedir.mkpath(destination.absoluteDir().absolutePath());

        //QThread::usleep(50000);
        if (copyFile(finfo.absoluteFilePath(), destPrefix + relativePart) == false) {
            qDebug() << "copy" << finfo.absoluteFilePath() << "to" << destPrefix + relativePart << "failed";
            return FILE_AREADY_EXISTS;
        }
        // copiedBytes are updated in byteWritten slot
        copiedFiles++;
        float percent = (((float)copiedBytes / (float)totalBytes) * 100.0f);
        emit copyProgress(copiedFiles, (int)percent, currentFileName);
    }
    return RESULT_OK;
}

FileCopyHandler::FileCopyHandlerResult FileCopyHandler::internalCopyToIntMemory(QDir sourceDirectory)
{

    QList<QFileInfo> copyList;
    Settings *settings = qApp->property("settings").value<Settings *>();

    if (!sourceDirectory.exists())
        return RESULT_SOURCE_DIR_NOT_EXIST;
    if (!settings->getStickAttached())
        return MEMORY_STICK_NOT_ATTACHED;
    QDir mountDir(extMediaMountPoint);
    if (!mountDir.exists() || !mountDir.Readable)
        return MEMORY_STICK_NOT_WRITABLE;

    QString destPrefix = intMediaMountPoint + sourceDirectory.dirName() + "/";
    QDir dDir(destPrefix);
    if (dDir.exists())
        return DIR_ALREADY_EXISTS;

    getMediaFilesRecursively(sourceDirectory, copyList);
    emit copyMetrics(totalFiles, totalBytes);

    quint64 freeSpace = Utils::getFreeSpace(intMediaMountPoint);
    if (freeSpace < totalBytes)
        return NOT_ENOUGH_FREE_SPACE;

    foreach (QFileInfo finfo, copyList)
    {
        currentFileName = finfo.baseName();
        QString relativePart = sourceDirectory.relativeFilePath(finfo.absoluteFilePath());
        QFileInfo destination(destPrefix + relativePart);
        // make necessary directories
        QDir filedir(destination.absoluteDir());
        if (!filedir.exists())
            filedir.mkpath(destination.absoluteDir().absolutePath());

        //QThread::usleep(50000);
        if (copyFile(finfo.absoluteFilePath(), destPrefix + relativePart) == false) {
            qDebug() << "copy" << finfo.absoluteFilePath() << "to" << destPrefix + relativePart << "failed";
            return FILE_AREADY_EXISTS;
        }
        copiedFiles++;
        //float percent = ((float)copiedBytes / (float)totalBytes) * 100.0f;
        //emit copyProgress(copiedFiles, (int)percent, currentFileName);
    }
    return RESULT_OK;
}

void FileCopyHandler::getMediaFilesRecursively(QDir sourceDirectory, QList<QFileInfo> &list)
{
    QStringList filters;
    QFileInfo fileinfo;
    filters << "*.jpg" << "*.bmp" << "*.png" << "*.mpg" << "*.mpeg4" << "*.mp4" << "*.avi" << "*.mov" << "*.mkv";
    foreach (QString file, sourceDirectory.entryList(filters)) {
        fileinfo.setFile(sourceDirectory.absoluteFilePath(file));
        if (fileinfo.isDir() && !fileinfo.isSymLink() && fileinfo.baseName() != ".." && fileinfo.baseName() != ".")
            getMediaFilesRecursively(fileinfo.absoluteDir(), list);
        totalBytes += fileinfo.size();
        totalFiles++;
        list.append(fileinfo);
    }
}

void FileCopyHandler::bytesWritten(qint64 written)
{
    copiedBytes += written;
    float percent = ((float)copiedBytes / (float)totalBytes) * 100.0f;
    emit copyProgress(copiedFiles, (int)percent, currentFileName);
}

void FileCopyHandler::CopyFinished()
{
    emit copyDone(future.result());
}

bool FileCopyHandler::copyFile(QString from, QString to)
{
    QByteArray bytearr;
    bool ret = true;
    QFile fromFile(from);
    QFile toFile(to);

    if (fromFile.open(QIODevice::ReadOnly) == false)
        return false;
    if (toFile.open(QIODevice::WriteOnly) == false) {
        fromFile.close();
        return false;
    }
    quint64 totalCopied = 0;
    quint64 totalSize = fromFile.size();
    while (totalCopied < totalSize) {
        bytearr = fromFile.read(32768);
        if (bytearr.length() == 0) {
            ret = false;
            break;
        }
        toFile.write(bytearr);
        totalCopied += bytearr.length();
        this->bytesWritten(bytearr.length());
    }

    fromFile.close();
    toFile.close();
    return ret;
}
