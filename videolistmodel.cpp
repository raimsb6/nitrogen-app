
#include "videolistmodel.h"

VideoListModel::VideoListModel(QObject *parent) :
    QAbstractListModel(parent)
{
    discoverer = QGst::Discoverer::create(100);
}

QHash<int, QByteArray> VideoListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NameRole] = "name";
    roles[UrlRole] = "url";
    roles[WidthRole] = "widthPixels";
    roles[HeightRole] = "heightPixels";
    roles[BitrateRole] = "bitrate";
    roles[LengthRole] = "lengthSec";
    roles[SizeRole] = "sizeMb";
    roles[FramerateRole] = "framerate";
    roles[VideoCodecRole] = "videoCodec";
    roles[AudioCodecRole] = "audioCodec";
    roles[ContainerRole] = "container";
    return roles;
}

int VideoListModel::rowCount(const QModelIndex &) const
{
    return entryList.count();
}

QVariant VideoListModel::data(const QModelIndex & index, int role) const
{
    VideoData *videoData = entryList.at(index.row());
    if (role == NameRole)
        return QVariant(videoData->name);
    else if (role == UrlRole)
        return QVariant(videoData->fileUrl);
    else if (role == WidthRole)
        return QVariant(videoData->widthtPixels);
    else if (role == HeightRole)
        return QVariant(videoData->heightPixels);
    else if (role == BitrateRole)
        return QVariant(videoData->bitrate);
    else if (role == LengthRole)
        return QVariant(videoData->lengthSec);
    else if (role == SizeRole)
        return QVariant(videoData->sizeMb);
    else if (role == FramerateRole)
        return QVariant(videoData->framerate);
    else if (role == VideoCodecRole)
        return QVariant(videoData->videoCodec);
    else if (role == AudioCodecRole)
        return QVariant(videoData->AudioCodec);
    else if (role == ContainerRole)
        return QVariant(videoData->Container);
    return QVariant();
}

QString VideoListModel::getPath()
{
    return fullPath;
}

QString VideoListModel::getTitle()
{
    return title;
}

void VideoListModel::setTitle(const QString &newTitle)
{
   if (newTitle != title) {
       title = newTitle;
       emit titleChanged();
   }
}

void VideoListModel::setPath(const QString &path)
{
    if (path != fullPath)
    {
       beginResetModel();
       fullPath = path;
       QDir dir;
       QStringList filters;
       filters << "*.mpg" << "*.mpeg4" << "*.mp4" << "*.avi" << "*.mov" << "*.mkv";
       dir.setPath(fullPath);
       setTitle(dir.dirName());
       qDeleteAll(entryList);
       entryList.clear();
       QGst::DiscovererInfoPtr discovererInfo;
       foreach (QString file, dir.entryList(filters)) {
           discovererInfo = discoverer->discoverUri("file://" + fullPath + file);

           QFileInfo info(fullPath + file);
           VideoData *video = new VideoData();
           video->lengthSec = discovererInfo->duration().toTime().toString();
           foreach (QGst::DiscovererStreamInfoPtr stream, discovererInfo->videoStreams()) {
               QGst::DiscovererVideoInfoPtr videoStream = stream.dynamicCast<QGst::DiscovererVideoInfo>();
               float ftmp = (float)videoStream->bitrate() / (float)(1024 * 1024);
               video->bitrate = QString::number(ftmp, 'f', 2);
               video->heightPixels = videoStream->height();
               video->widthtPixels = videoStream->width();
               QGst::Fraction framerate = videoStream->framerate();
               ftmp = (float)framerate.numerator / (float)framerate.denominator;
               video->framerate = QString::number(ftmp, 'f', 4);
               video->videoCodec = videoStream->tags().videoCodec();
               video->Container = videoStream->tags().containerFormat();
           }
           foreach (QGst::DiscovererStreamInfoPtr stream, discovererInfo->audioStreams()) {
               QGst::DiscovererAudioInfoPtr audioStream = stream.dynamicCast<QGst::DiscovererAudioInfo>();
               video->AudioCodec = audioStream->tags().audioCodec();
           }

           video->name = info.baseName().left(20);
           video->fileUrl = fullPath + file;
           video->sizeMb = info.size() / (1024 * 1024);

           entryList.append(video);
       }
       endResetModel();
       emit pathChanged();

    }
}
