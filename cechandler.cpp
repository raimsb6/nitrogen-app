#include <QThread>
#include <QDebug>
#include <QDateTime>
#include <QKeyEvent>
#include <QApplication>
#include "cechandler.h"

using namespace CEC;

CecHandler::CecHandler(QObject *parent) :
    QObject(parent)
{
    m_adapter = NULL;
    connect(&futureWatcher, SIGNAL(finished()), this, SLOT(CecConnectionOpenFinished()));
}

CecHandler::~CecHandler()
{
    Deinit();
}

void CecHandler::Deinit()
{
    if (m_adapter)
    {
        qDebug() << "Closing CEC";
        m_adapter->Close();
        CECDestroy(m_adapter);
    }
    m_adapter = NULL;
}

bool CecHandler::Init(QObject *viewer)
{
    viewerObj = viewer;
    libcec_configuration cecConf;
    cecConf.clientVersion = CEC_CLIENT_VERSION_CURRENT;
    // device name
    QByteArray arr = QString("nitrogen6x").toLocal8Bit();
    memcpy(cecConf.strDeviceName, arr.constData(), arr.count());
    // device type
    cecConf.deviceTypes.Add(CEC_DEVICE_TYPE_PLAYBACK_DEVICE);
    // try to autodetect the address
    cecConf.bAutodetectAddress = CEC_DEFAULT_SETTING_AUTODETECT_ADDRESS;
    cecConf.iPhysicalAddress = CEC_PHYSICAL_ADDRESS_TV;
    cecConf.baseDevice = CECDEVICE_TV;
    m_callbacks.CBCecLogMessage = &cecLogMessage;
    m_callbacks.CBCecKeyPress = &cecKeyPress;
    m_callbacks.CBCecCommand = &cecCommand;
    m_callbacks.CBCecConfigurationChanged = &cecConfChange;
    m_callbacks.CBCecAlert = &cecAlert;
    m_callbacks.CBCecMenuStateChanged = &cecMenuStateChanged;
    m_callbacks.CBCecSourceActivated = &cecSourceActivated;
    cecConf.callbackParam = this;
    cecConf.callbacks = &m_callbacks;

    void *ptr = CECInitialise(&cecConf);
    if (!ptr)
        return false;
    m_adapter = static_cast<ICECAdapter *>(ptr);
    future = QtConcurrent::run(this, &CecHandler::OpenAdapter, (const QString&)"i.MX");
    futureWatcher.setFuture(future);
    return true;
}

bool CecHandler::OpenAdapter(QString port)
{
    if (m_adapter->Open(port.toStdString().c_str()) == false)
    {
        qDebug() << "could not open ECE adapter";
        return false;
    }
    qDebug() << "ECE adapter opened";
    return true;
}

void CecHandler::CecConnectionOpenFinished()
{
    if (future.result() == false)
        emit CecConnectionFailed();
    else
    {
        emit CecConnectionEstablished("Sony");
    }
}

QObject *CecHandler::getViewer()
{
    return viewerObj;
}

int CecHandler::cecLogMessage(void *ptr, const cec_log_message msg)
{
    Q_UNUSED(ptr);
    QDateTime time;
    QString message(msg.message);
    QString loglevel;
    time.setTime_t(msg.time);

    switch (msg.level)
    {
    case CEC_LOG_ERROR:
        loglevel = "ERR";
        break;
    case CEC_LOG_WARNING:
        loglevel = "WARN";
        break;
    case CEC_LOG_DEBUG:
        loglevel = "DEBUG";
        break;
    case CEC_LOG_NOTICE:
        loglevel = "NOTICE";
        break;
    case CEC_LOG_TRAFFIC:
        loglevel = "TRAFFIC";
        break;
    case CEC_LOG_ALL:
        loglevel = "ALL";
        break;
    }

    qDebug() << time.toString(Qt::SystemLocaleShortDate) << loglevel << ": " << message;
    return 1;
}

int CecHandler::cecKeyPress(void *ptr, const cec_keypress press)
{
    qDebug() << "key press code" << press.keycode << "; duration" << press.duration;
    CecHandler *thisPtr = (CecHandler *)ptr;
    int key = 0;

    // don't act on key release
    if (press.duration != 0)
        return 1;

    switch (press.keycode)
    {
     case CEC_USER_CONTROL_CODE_LEFT:
        key = Qt::Key_Left;
        break;
    case CEC_USER_CONTROL_CODE_RIGHT:
        key = Qt::Key_Right;
        break;
    case CEC_USER_CONTROL_CODE_UP:
        key = Qt::Key_Up;
        break;
    case CEC_USER_CONTROL_CODE_DOWN:
        key = Qt::Key_Down;
        break;
    case CEC_USER_CONTROL_CODE_SELECT:
        key = Qt::Key_Return;
        break;
    case CEC_USER_CONTROL_CODE_BACKWARD:
    case CEC_USER_CONTROL_CODE_EXIT:
        key = Qt::Key_Escape;
        break;
    case CEC_USER_CONTROL_CODE_F1_BLUE:
        key = Qt::Key_4;
        break;
    case CEC_USER_CONTROL_CODE_F2_RED:
        key = Qt::Key_1;
        break;
    case CEC_USER_CONTROL_CODE_F3_GREEN:
        key = Qt::Key_2;
        break;
    case CEC_USER_CONTROL_CODE_F4_YELLOW:
        key = Qt::Key_3;
        break;
    case CEC_USER_CONTROL_CODE_PLAY:
        key = Qt::Key_4;
        break;
    case CEC_USER_CONTROL_CODE_PAUSE:
        key = Qt::Key_5;
        break;
    case CEC_USER_CONTROL_CODE_STOP:
        key = Qt::Key_6;
        break;
    case CEC_USER_CONTROL_CODE_FAST_FORWARD:
        key = Qt::Key_7;
        break;
    case CEC_USER_CONTROL_CODE_REWIND:
        key = Qt::Key_8;
        break;
    default:
        break;
    }

    if (key)
        postKeyPress(thisPtr->getViewer(), key);
    return 1;
}

int CecHandler::cecCommand(void *ptr, const cec_command cmd)
{
    CecHandler *thisPtr = (CecHandler *)ptr;
    qDebug() << "CEC command" << cmd.opcode;

    if (cmd.opcode == CEC_OPCODE_PLAY)
    {
        if (cmd.initiator == CECDEVICE_TV && cmd.parameters.size == 1)
        {
            if (cmd.parameters[0] == CEC_PLAY_MODE_PLAY_FORWARD)
            {
                // play
                 postKeyPress(thisPtr->getViewer(), Qt::Key_4);
            }
            else if (cmd.parameters[0] == CEC_PLAY_MODE_PLAY_STILL)
            {
                // pause
                postKeyPress(thisPtr->getViewer(), Qt::Key_5);
            }
        }
    }
    else
    {
        qDebug() << "unhandled command";
    }

    return 1;
}

int CecHandler::cecConfChange(void *ptr, const libcec_configuration conf)
{
    Q_UNUSED(ptr);
    Q_UNUSED(conf);
    return 1;
}

void CecHandler::postKeyPress(QObject *receiver, int keycode)
{
    QKeyEvent *event = new QKeyEvent(QEvent::KeyPress, keycode, Qt::NoModifier);
    QApplication::postEvent(receiver, event);
}

int CecHandler::cecAlert(void *ptr, const libcec_alert alert, const libcec_parameter param)
{
    Q_UNUSED(ptr);
    Q_UNUSED(param);

    switch (alert)
    {
    case CEC_ALERT_SERVICE_DEVICE:
        qDebug() << "Alert service device";
        break;
    case CEC_ALERT_CONNECTION_LOST:
        qDebug() << "connection lost alert";
        break;
    case CEC_ALERT_TV_POLL_FAILED:
        qDebug() << "TV poll failed";
        break;
    case CEC_ALERT_PERMISSION_ERROR:
        qDebug() << "Alert permission error";
        break;
    case CEC_ALERT_PORT_BUSY:
        qDebug() << "port busy";
        break;
    case CEC_ALERT_PHYSICAL_ADDRESS_ERROR:
        qDebug() << "physical address error";
        break;
    default:
        qDebug() << "alert";
        break;
    }
    return 1;
}

int CecHandler::cecMenuStateChanged(void *ptr, const cec_menu_state state)
{
    Q_UNUSED(ptr);
    Q_UNUSED(state);

    qDebug() << "menu state changed";
    return 1;
}

void CecHandler::cecSourceActivated(void *ptr, const cec_logical_address addr, const uint8_t activated)
{
    Q_UNUSED(ptr);
    Q_UNUSED(addr);

    qDebug() << "source activated" << activated;
}
