#include <QtGui/QGuiApplication>
#include <QQmlContext>
#include <QtQml>
#include <QThread>
#include <QGst/Init>
#include <QtQuick/QQuickView>
#include "qtquick2applicationviewer.h"
#include "imagelistmodel.h"
#include "imagelistmodelsortproxy.h"
#include "folderlistmodel.h"
#include "videolistmodel.h"
#include "mainapp.h"
#include "filecopyhandler.h"
#include "advancedimage.h"

int main(int argc, char *argv[])
{
#if defined(QTVIDEOSINK_PATH)
    //this allows the example to run from the QtGStreamer build tree without installing QtGStreamer
    qputenv("GST_PLUGIN_PATH", QTVIDEOSINK_PATH);
#endif
    MainApp app(argc, argv);
    QGst::init(&argc, &argv);
    QtQuick2ApplicationViewer viewer;

    app.setViewerObject(&viewer);
    qmlRegisterType<GVideoPlayer>("GVideoPlayer", 1, 0, "GVideoPlayer");
    qmlRegisterType<Settings>("Settings", 1, 0, "Settings");
    qmlRegisterType<FileCopyHandler>("FileCopyHandler", 1, 0, "FileCopyHandler");
    qmlRegisterType<AdvancedImage>("AdvancedImage", 1, 0, "AdvancedImage");

    QQmlContext *context = viewer.rootContext();

    ImageListModel *model = new ImageListModel(context);
    ImageListModelSortProxy *proxy = new ImageListModelSortProxy(0);
    proxy->setSourceModel(model);
    proxy->setSortRole(ImageListModel::ModifiedRole);
    proxy->sort(0, Qt::AscendingOrder);

    context->setContextProperty("ImageModelTest", proxy);

    FolderListModel *foldermodel = new FolderListModel(context);
    context->setContextProperty("FolderModel", foldermodel);

    VideoListModel *videomodel = new VideoListModel(context);
    context->setContextProperty("VideoModel", videomodel);

    context->setContextProperty("GlobalSettings", &app.m_settings);

    viewer.setSource(QUrl("qrc:/qml/qml/main.qml"));
    viewer.showFullScreen();

    app.PrepareExec();
    int ret = app.exec();
    app.PrepareClose();
    return ret;
}
