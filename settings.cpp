#include <QDebug>
#include <QHostAddress>
#include <QNetworkInterface>
#include "settings.h"
#include "utils.h"

Settings::Settings(QObject *parent) :
    QObject(parent)
{
    persistentSettings = new QSettings("Nitrogen6x", "Meediakeskus");
    m_useHdmiControl = persistentSettings->value("HdmiControlEnabled").toBool();
    m_useNetworkControl = persistentSettings->value("NetworkControlEnabled").toBool();
    m_lastUsedSortOrder = persistentSettings->value("LastUsedSortOrder").toInt();
    m_slideshowTime = persistentSettings->value("SlideshowTime", 5).toInt();
    m_lastHdmiConnected = false;
    m_lastAndroidConnected = false;
}

Settings::~Settings()
{
    delete persistentSettings;
}

bool Settings::getStickAttached()
{
    return m_stickAttached;
}

void Settings::setStickAttached(bool stickAttached)
{
    if (stickAttached != m_stickAttached)
    {
        qDebug() << "setting stick status" << stickAttached;
        m_stickAttached = stickAttached;
        emit stickAttachedChanged();
    }
}

bool Settings::getUseHdmiControl()
{
    return m_useHdmiControl;
}

void Settings::setUseHdmiControl(bool useHdmiControl)
{
    if (m_useHdmiControl != useHdmiControl)
    {
        m_useHdmiControl = useHdmiControl;
        persistentSettings->setValue("HdmiControlEnabled", m_useHdmiControl);
        emit useHdmiControlChanged();
    }
}

bool Settings::getUseNetworkControl()
{
    return m_useNetworkControl;
}

void Settings::setUseNetworkControl(bool useNetworkControl)
{
    if (m_useNetworkControl != useNetworkControl)
    {
        m_useNetworkControl = useNetworkControl;
        persistentSettings->setValue("NetworkControlEnabled", m_useNetworkControl);
        emit useNetworkControlChanged();
    }
}

int Settings::getLastUsedSortOrder()
{
    return m_lastUsedSortOrder;
}

void Settings::setLastUsedSortOrder(int lastSortOrder)
{
    if (m_lastUsedSortOrder != lastSortOrder)
    {
        m_lastUsedSortOrder = lastSortOrder;
        persistentSettings->setValue("LastUsedSortOrder", m_lastUsedSortOrder);
        qDebug() << "last used sort now" << m_lastUsedSortOrder;
        emit lastUsedSortOrderChanged();
    }
}

int Settings::getSlideshowTime()
{
    return m_slideshowTime;
}

void Settings::setSlideshowTime(int time)
{
    if (m_slideshowTime != time)
    {
        m_slideshowTime = time;
        persistentSettings->setValue("SlideshowTime", m_slideshowTime);
        emit slideshowTimeChanged();
    }
}

QString Settings::getIpAddress()
{
    QString ret = "0.0.0.0";
    QNetworkInterface interface = QNetworkInterface::interfaceFromName("eth0");
    if (interface.isValid()) {
        foreach (const QHostAddress &address, interface.allAddresses()) {
            if (address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost))
                ret = address.toString();
        }
    }
    if (ret != m_lastIpAddress) {
        m_lastIpAddress = ret;
        emit ipAddressChanged();
    }

    return ret;
}

QString Settings::getNetmask()
{
    QString ret = "0.0.0.0";
    QNetworkInterface interface = QNetworkInterface::interfaceFromName("eth0");
    if (interface.isValid()) {
        foreach (const QNetworkAddressEntry &address, interface.addressEntries()) {
            ret = address.netmask().toString();
        }
    }
    if (ret != m_lastNetmask) {
        m_lastNetmask = ret;
        emit netmaskChanged();
    }
    return ret;
}

QString Settings::getGateway()
{
    QString ret = Utils::getDefaultGateway().toString();
    if (ret != m_lastGateway) {
        m_lastGateway = ret;
        emit gatewayChanged();
    }
    return ret;
}

bool Settings::getNetworkUp()
{
    bool ret;
    QNetworkInterface interface = QNetworkInterface::interfaceFromName("eth0");
    if (interface.isValid() && interface.flags() & QNetworkInterface::IsRunning)
        ret = true;
    else
        ret = false;
    if (ret != m_lastNetworkUp) {
        m_lastNetworkUp = ret;
        emit networkUpChanged();
    }
    return ret;
}

QString Settings::getMacAddress()
{
    QNetworkInterface interface = QNetworkInterface::interfaceFromName("eth0");
    if (interface.isValid())
        return interface.hardwareAddress();
    else
        return "";
}

bool Settings::getHdmiConnected()
{
    return m_lastHdmiConnected;
}

void Settings::hdmiConnectedStatusUpdate(bool connected)
{
    if (connected != m_lastHdmiConnected) {
        m_lastHdmiConnected = connected;
        emit hdmiConnectedChanged();
    }
}

bool Settings::getAndroidConnected()
{
    return m_lastAndroidConnected;
}

void Settings::androidConnectedStatusUpdate(bool connected)
{
    if (connected != m_lastAndroidConnected) {
        m_lastAndroidConnected = connected;
        emit androidConnectedChanged();
    }
}

void Settings::refresh()
{
    getNetworkUp();
    getIpAddress();
    getNetmask();
    getGateway();
}
