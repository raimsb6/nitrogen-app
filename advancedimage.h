#ifndef ADVANCEDIMAGE_H
#define ADVANCEDIMAGE_H

#include <QQuickPaintedItem>
#include <QPainter>
#include <QPixmap>
#include <QFuture>
#include <QFutureWatcher>
#include <QtConcurrent>

class AdvancedImage : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(QString source READ getSource WRITE setSource NOTIFY sourceChanged)

public:
    explicit AdvancedImage(QQuickItem *parent = 0);
    void paint(QPainter *painter);
    QString getSource();

private:
    QString m_source;
    QPixmap m_pixmap;
    bool isLoading;
    QFuture<void> m_future;
    QFutureWatcher<void> m_futurewatcher;
    void scaleImage();

signals:
    void sourceChanged();

public slots:
    void setSource(QString newSource);
    void scaleDone();

};

#endif // ADVANCEDIMAGE_H
