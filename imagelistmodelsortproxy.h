#ifndef IMAGELISTMODELSORTPROXY_H
#define IMAGELISTMODELSORTPROXY_H

#include <QSortFilterProxyModel>

class ImageListModelSortProxy : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(QString path READ getPath WRITE setPath NOTIFY pathChanged)
    Q_PROPERTY(QString title READ getTitle WRITE setTitle NOTIFY titleChanged)
    Q_PROPERTY(int sortOrder READ getSortOrder WRITE setSortOrder)

public:
    Q_INVOKABLE QString getNextUrl(int index);

    ImageListModelSortProxy(QObject *parent);
    int columnCount(const QModelIndex&) const;

    QString getPath();
    QString getTitle();
    int getSortOrder();
    QString getNextUrl();

protected:

public Q_SLOTS:
    void setPath(const QString &);
    void setTitle(const QString &);
    void setSortOrder(int order);

Q_SIGNALS:
    void pathChanged();
    void titleChanged();
};

#endif // IMAGELISTMODELSORTPROXY_H
