#include <QApplication>
#include <QKeyEvent>
#include <QDataStream>
#include "tcphandler.h"

TcpHandler::TcpHandler(QObject *parent) :
    QObject(parent)
{
    socket = NULL;
}

void TcpHandler::Init(QObject *receiver)
{
    connected = false;
    keyReceiver = receiver;
    connect(&server, SIGNAL(newConnection()), this, SLOT(newConnection()));
    server.listen(QHostAddress::Any, 54321);
}

void TcpHandler::Deinit()
{
    if (socket)
    {
        socket->close();
        socket->disconnect();
    }
    server.close();
    server.disconnect();
}

void TcpHandler::newConnection()
{
    qDebug() << "new connection";
    socket = server.nextPendingConnection();
    if (socket == NULL)
        return;
    connected = true;
    connect(socket, SIGNAL(readyRead()), this, SLOT(dataReady()));
    connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(socketError()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(disconnectedConnection()));
    emit connectionOpened(socket->peerAddress().toString());
}

void TcpHandler::disconnectedConnection()
{
    socket = NULL;
    emit connectionClosed();
}

void TcpHandler::dataReady()
{
    QByteArray arr;
    arr = socket->read(2);
    if (arr.at(0) != 0xaa)
    {
        qDebug() << "no sync byte";
        socket->flush();
        return;
    }
    quint8 len = arr.at(1);
    arr = socket->read(len);
    if (arr.length() < len)
    {
        qDebug() << "not enough byte in msg";
        return;
    }
    handleMsg(arr);
}

void TcpHandler::socketError()
{
    connected = false;
    socket->close();
    socket = NULL;
}

void TcpHandler::handleMsg(QByteArray msg)
{
    QDataStream stream(msg);
    quint8 cmd;
    stream >> cmd;
    if (cmd == 2) {
        quint16 addr;
        stream >> addr;
        if (addr == 1)
        {
            quint32 data;
            stream >> data;
            int key = 0;
            switch (data)
            {
            case 1:
                key = Qt::Key_1;
                break;
            case 2:
                key = Qt::Key_2;
                break;
            case 3:
                key = Qt::Key_3;
                break;
            case 4:
                key = Qt::Key_4;
                break;
            case 5:
                key = Qt::Key_Escape;
                break;
            case 6:
                key = Qt::Key_Return;
                break;
            case 7:
                key = Qt::Key_Up;
                break;
            case 8:
                key = Qt::Key_Down;
                break;
            case 9:
                key = Qt::Key_Left;
                break;
            case 10:
                key = Qt::Key_Right;
                break;
            }
            QKeyEvent *event = new QKeyEvent(QEvent::KeyPress, key, Qt::NoModifier);
            QApplication::postEvent(keyReceiver, event);

        }
    }
}
