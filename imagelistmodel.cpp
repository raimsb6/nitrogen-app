#include <QImage>
#include <libexif/exif-data.h>
#include "imagelistmodel.h"

ImageData *scale(ImageData *imageData)
{
    QImage image(imageData->url);
    imageData->widthPixels = image.width();
    imageData->heightPixels = image.height();
    QString name = "/var/volatile/tmp/" + imageData->label + "_tn.jpg";
    image = image.scaled(QSize(600, 400), Qt::KeepAspectRatio).scaled(QSize(300, 200), Qt::KeepAspectRatio, Qt::SmoothTransformation);
    image.save(name);
    imageData->thumbnail = "file:///" + name;
    return imageData;
}


ImageListModel::ImageListModel(QObject *parent) :
    QAbstractListModel(parent)
{
    imageScaling = new QFutureWatcher<ImageData *>(this);
    connect(imageScaling, SIGNAL(resultReadyAt(int)), SLOT(showImage(int)));
    connect(imageScaling, SIGNAL(finished()), SLOT(finished()));
}

void ImageListModel::showImage(int num)
{
    QModelIndex index = this->index(num);
    emit dataChanged(index, index);
}

QHash<int, QByteArray> ImageListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NameRole] = "name";
    roles[UrlRole] = "url";
    roles[ThumbRole] = "thumb";
    roles[ModifiedRole] = "modified";
    roles[WidthRole] = "widthPixels";
    roles[HeightRole] = "heightPixels";
    roles[TakenRole] = "exifTaken";
    return roles;
}

int ImageListModel::rowCount(const QModelIndex &) const
{
    return entryList.count();
}

void ImageListModel::finished()
{
    qDebug() << "finished";
}

QVariant ImageListModel::data(const QModelIndex & index, int role) const
{
    ImageData *imageData = entryList.at(index.row());
    if (role == NameRole)
        return QVariant(imageData->label);
    else if (role == UrlRole)
        return QVariant(imageData->url);
    else if (role == ModifiedRole)
        return QVariant(imageData->modified);
    else if (role == TakenRole)
        return QVariant(imageData->exifTaken);
    else if (role == WidthRole)
        return QVariant(imageData->widthPixels);
    else if (role == HeightRole)
        return QVariant(imageData->heightPixels);
    else if (role == ThumbRole) {
        if (!imageData->thumbnail.length()) {
        }
        return QVariant(imageData->thumbnail);
    }
    return QVariant();
}

 QString ImageListModel::getPath()
 {
     return fullPath;
 }

 void ImageListModel::setTitle(const QString &newTitle)
 {
    if (newTitle != title) {
        title = newTitle;
        emit titleChanged();
    }
 }

 QString ImageListModel::getTitle()
 {
     return title;
 }

 void ImageListModel::setPath(const QString &path)
 {
     ExifData *ed;

     if (path != fullPath)
     {
        beginResetModel();
        fullPath = path;
        QDir dir;
        QStringList filters;
        filters << "*.jpg" << "*.bmp" << "*.png";
        dir.setPath(fullPath);
        setTitle(dir.dirName());
        qDeleteAll(entryList);
        entryList.clear();
        foreach (QString file, dir.entryList(filters)) {
            QFileInfo info(fullPath + file);
            ImageData *image = new ImageData();
            image->label = info.baseName().left(20);
            image->url = fullPath + file;
            ed = exif_data_new_from_file(image->url.toLocal8Bit());
            if (ed) {
                ExifEntry *entry = exif_content_get_entry(ed->ifd[EXIF_IFD_EXIF], EXIF_TAG_DATE_TIME_ORIGINAL);
                if (entry) {
                    char buf[256];
                    exif_entry_get_value(entry, buf, sizeof(buf));
                    QString date = QString::fromLocal8Bit(buf);
                    image->exifTaken = QDateTime::fromString(date, "yyyy:MM:dd HH:mm:ss");
                }
            }
            image->thumbnail = "qrc:/images/images/photo.png";
            image->modified = info.lastModified();
            entryList.append(image);
        }
        imageScaling->setFuture(QtConcurrent::mapped(entryList, scale));
        endResetModel();
        emit pathChanged();

     }
 }

