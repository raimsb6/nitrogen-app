#include <sys/statfs.h>
#include <stdio.h>
#include <string.h>
#include <net/route.h>
#include <QString>
#include "utils.h"

Utils::Utils()
{
}

quint64 Utils::getFreeSpace(QString mount)
{
    struct statfs64 sfs;
    int ret = statfs64(mount.toStdString().c_str(), &sfs);
    if (ret < 0)
        return 0;
    return sfs.f_blocks * sfs.f_bfree;
}

QHostAddress Utils::getDefaultGateway()
{
    QHostAddress addr;
    addr.setAddress("0.0.0.0");
    FILE* routing_db = fopen("/proc/net/route", "ra");
    if (!routing_db)
        return addr;

    // Get default route
    while (!feof(routing_db))
    {
        // A line is actually 128 char's long
        char line[130] = {0};

        if (fgets(line, sizeof(line) - 1, routing_db) == NULL)
            continue;

        if (strstr(line, "eth0"))
        {
            unsigned int	Destination, Gateway, Mask, RefCnt, Use, MTU, Window;
            unsigned short	Flags, Metric, IRTT;

            sscanf(line + sizeof("eth0") + 1, " %08X %08X %04hu %u %u %hu %08X %u %u %hu ",
                &Destination, &Gateway, &Flags, &RefCnt, &Use, &Metric, &Mask, &MTU, &Window, &IRTT);

            if (Flags & RTF_GATEWAY && (Destination == 0x00000000) && (Mask == 0x00000000))
            {
                addr.setAddress(ntohl(Gateway));
                break;
            }
        }
    }
    fclose(routing_db);

    return addr;
}
