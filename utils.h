#ifndef UTILS_H
#define UTILS_H

#include <QHostAddress>

class Utils
{
public:
    Utils();
    static quint64 getFreeSpace(QString mount);
    static QHostAddress getDefaultGateway();
};

#endif // UTILS_H
