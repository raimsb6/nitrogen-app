#ifndef FILECOPYHANDLER_H
#define FILECOPYHANDLER_H

#include <QObject>
#include <QFuture>
#include <QFutureWatcher>
#include <QDir>

class FileCopyHandler : public QObject
{
    Q_OBJECT

public:
    Q_INVOKABLE void copyDirToExternalMemoryStart(QString sourceDir);
    Q_INVOKABLE void copyDirFromExternalMemoryStart(QString destDir);
    Q_ENUMS(FileCopyHandlerResult)
    explicit FileCopyHandler(QObject *parent = 0);

    enum FileCopyHandlerResult {
        RESULT_OK = 0,
        RESULT_SOURCE_DIR_NOT_EXIST,
        MEMORY_STICK_NOT_ATTACHED,
        MEMORY_STICK_NOT_WRITABLE,
        NOT_ENOUGH_FREE_SPACE,
        DIR_ALREADY_EXISTS,
        FILE_AREADY_EXISTS
    };

private:
    int totalFiles;
    quint64 totalBytes;
    int copiedFiles;
    quint64 copiedBytes;
    QString currentFileName;
    // 0 - not running; 1 - copying to memory stick; 2 - copying from memory stick
    int running;
    QFuture<FileCopyHandlerResult> future;
    QFutureWatcher<FileCopyHandlerResult> futureWatcher;

    FileCopyHandlerResult internalCopyToExtMemory(QDir sourceDirectory);
    FileCopyHandlerResult internalCopyToIntMemory(QDir sourceDirectory);
    void getMediaFilesRecursively(QDir sourceDirectory, QList<QFileInfo> &list);
    bool copyFile(QString from, QString to);

signals:
    void copyMetrics(int numFiles, quint64 numBytes);
    void copyProgress(int filesCopied, int percent, const QString &currentFile);
    void copyDone(FileCopyHandlerResult copyStatus);

public slots:
private slots:
    void bytesWritten(qint64 written);
    void CopyFinished();

};

#endif // FILECOPYHANDLER_H
